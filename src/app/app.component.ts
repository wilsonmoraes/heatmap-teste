import {Component, OnInit} from '@angular/core';

declare const google;
declare const jQuery: any;
declare const HeatmapOverlay: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'heatmap-test';

  ngOnInit(): void {
    const DAY_IN_MILLI = 1000 * 60 * 60 * 24;

    const userAgent = navigator.userAgent.toLowerCase();

    const isMobile = typeof window.orientation !== 'undefined' && !userAgent.match(/iPad/i);
    const isBrowserFast = /webkit/.test(userAgent);

    if (userAgent.indexOf('msie') >= 0 ? parseInt(navigator.userAgent.toLowerCase().split('msie')[1]) < 9 : false) {
      alert('Sorry friend,\n but that browser is too old to learn new tricks,\n try Firefox, you will like it.');
    }


    /*
      * emits events after each animation frame and after animation is complete
     */
    const CrimeAnimator = function (data, heatmap, map) {
      console.log(data);
      this.map = map;
      this.data = data;
      this.heatmap = heatmap;
      this.dataLength = data.length - 1;

      //  create marker array to track displayed markers and the info window that displays when they are clicked
      this.markers = [];
      this.showMarkers = true;
      this.markerInfoWindow = new google.maps.InfoWindow();

      //  figure out the date range
      this.dateRange = {start: data[0][3], end: data[data.length - 1][3]};
      this.dayRange = Math.ceil((this.dateRange.end - this.dateRange.start) / DAY_IN_MILLI);

      this.isPaused = false;

      //  calculate the number of days that elapse for each animation frame
      this.animationTimeMultiplier = 500000 * (this.dayRange / 365);

      //  calculate how long crimes should be displayed for based on our range
      this.visibleLifespan = Math.ceil(this.dayRange / 10);
      if (this.visibleLifespan < 10) {
        this.visibleLifespan = 10;
      } else if (this.visibleLifespan > 30) {
        this.visibleLifespan = 30;
      }
      this.visibleLifespan = this.visibleLifespan * DAY_IN_MILLI;

      this.lowIndex = 0; //  holds the data index of the oldest crime we are displaying
      this.highIndex = 0; //  holds the data index of the newest crime we are displaying

      this.filters = [];

      this.setupProgressBar();

      const self = this;
      const $animationControlButton = jQuery('#animation-control-button');
      $animationControlButton.click(this.togglePause.bind(this));

      //  hookup spacebar pauser/resume
      jQuery(window).keypress(this.togglePause.bind(this));

      this.$animationControl = $animationControlButton.children('div');

      //  if the animation is paused then redraw the current frame on drag end
      google.maps.event.addListener(map, 'dragend', function () {

        if (self.isPaused) {
          self.drawFrame(self.currentDate);
        }

      });

      google.maps.event.addListener(map, 'zoom_changed', function () {
        let radiusMultiplier;
        const zoom = map.getZoom();
        if (zoom > 10) {
          radiusMultiplier = 2.6;
        } else if (zoom < 9) {
          radiusMultiplier = 1;
        } else {
          radiusMultiplier = 1.5;
        }

        heatmap.heatmap.set('radius', zoom * radiusMultiplier);

        if (self.isPaused) {
          self.drawFrame(self.currentDate);
        }
      });
    };

    CrimeAnimator.prototype.setupProgressBar = function setupProgressBar() {

      //  setup progress bar slider events
      this.$progressBar = jQuery('#progress-bar-container');
      const $progressBarPointer = jQuery('#progress-bar-pointer');
      const $document = jQuery(document);

      this.progressBarStartPosition = Math.ceil($document.width() * 0.05);
      this.progressBarEndPosition = $document.width() - this.progressBarStartPosition;

      this.$progressBar.css('width', this.progressBarStartPosition + 'px');

      this.totalTimeRange = this.dateRange.end - this.dateRange.start;

      this.totalPixelRange = this.progressBarEndPosition - this.progressBarStartPosition;

      const self = this;
      const stopPointerDrag = function (e) {
        self.updateCurrentDateToProgressPosition(e.pageX);

        // draw the heatmap frame for our current position
        self.lowIndex = 0;
        self.highIndex = 0;
        self.drawFrame(self.currentDate);

        self.removeCrimeMarkers();
        if (self.showMarkers) {
          self.addCrimeMarkers();
        }

        $document.off('mousemove ontouchmove');
        e.preventDefault();
      };

      // keep track of the top of the progres bar so that we can stop dragging when the mouse leaves the top
      let pointerOffsetTop = $progressBarPointer.offset().top;
      jQuery(window).resize(function () {
        pointerOffsetTop = $progressBarPointer.offset().top;
      });

      // setup dragability of progress bar pointer
      $progressBarPointer.on('mousedown ontouchstart', function (e) {
        self.pause.apply(self);

        $document.on('mousemove ontouchmove', function (e) {
          if (e.pageY < pointerOffsetTop) {
            stopPointerDrag(e);
            return;
          }

          // impose start/end limits on progres bar
          let x = e.pageX;
          if (x > self.progressBarEndPosition) {
            x = self.progressBarEndPosition;
          } else if (x < self.progressBarStartPosition) {
            x = self.progressBarStartPosition;
          }

          self.$progressBar.css('width', x + 'px');

          // update progress bar text
          self.updateCurrentDateToProgressPosition(x);
          self.updateProgressBarText();

          self.removeCrimeMarkers();

          // in chrome we will animate on drag
          if (isBrowserFast) {
            self.drawFrame(self.currentDate);
          }

          e.preventDefault();
        });

        e.preventDefault();
      });

      // stop dragging
      $progressBarPointer.on('mouseup ontouchend', stopPointerDrag);

      this.$progressBarPointer = $progressBarPointer;
    };

    const monthNames = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    CrimeAnimator.prototype.updateProgressBar = function updateProgressBar() {
      let x = (this.progressBarStartPosition + (((this.currentDate - this.dateRange.start) / this.totalTimeRange) * this.totalPixelRange));
      if (x > this.progressBarEndPosition) {
        x = this.progressBarEndPosition;
      }

      this.$progressBar.css('width', x + 'px');

      this.updateProgressBarText();
    };

    CrimeAnimator.prototype.updateCurrentDateToProgressPosition = function updateCurrentDateToProgressPosition(progressPosition) {
      // calc the current position of pointer and translate that into an animation position date
      const progressBarPercentComplete = (progressPosition - this.progressBarStartPosition) / this.totalPixelRange;
      this.currentDate = this.dateRange.start + ((this.dateRange.end - this.dateRange.start) * progressBarPercentComplete);
    };

    CrimeAnimator.prototype.updateProgressBarText = function updateProgressBarText() {
      const now = new Date(this.currentDate);
      this.$progressBarPointer.html(monthNames[now.getMonth()] + '<br>' + now.getDate() +
        '<span class=date-prefix>' + getNumberPostfix(now.getDate()) + '</span><br>' +
        '<span class=year>- ' + now.getFullYear() + ' -</span><br>');
    };

    const numberEndings = ['', 'st', 'nd', 'rd', 'th'];

    function getNumberPostfix(number) {
      return numberEndings[number >= numberEndings.length ? numberEndings.length - 1 : number];
    }

    CrimeAnimator.prototype.applyFilters = function setData(filters) {
      this.filters = filters;
    };

    CrimeAnimator.prototype.updateIndexPositions = function updateIndexPositions(visibleThreshold, nextDate) {
      this.lowIndex = this.getNewIndexPosition(this.lowIndex, visibleThreshold, 0);

      if (this.highIndex < this.lowIndex) {
        this.highIndex = this.lowIndex;
      }

      this.highIndex = this.getNewIndexPosition(this.highIndex, nextDate, this.lowIndex);
    },

      /**
       adjust index position to match the comparitor date and the current data
       */
      CrimeAnimator.prototype.getNewIndexPosition = function getNewIndexPosition(index, comparitorDate, indexMinimum) {
        // expand the index till it reaches the compairitor date
        while (index < this.dataLength) {
          if (this.data[index][3] >= comparitorDate) {
            break;
          }

          index += 1;
        }

        // contract the index till it reaches the compairitor date
        while (index > indexMinimum) {
          if (this.data[index][3] < comparitorDate) {
            break;
          }

          index -= 1;
        }

        return index;
      };

    CrimeAnimator.prototype.drawFrame = function drawFrame(nextDate) {
      const visibleThreshold = this.currentDate - this.visibleLifespan;
      this.updateIndexPositions(visibleThreshold, nextDate);

      // if no removals to make then just add the crime points that are between the currentDate and the next Date and aren't filtered
      const currentData = this.getCurrentHeatmapData(this.lowIndex, this.highIndex, visibleThreshold);
      this.heatmap.setDataSet(currentData);
    };

    CrimeAnimator.prototype.animate = function animate() {

      const now = new Date().getTime();

      if (!this.isPaused) {
        const nextDate = this.currentDate + ((now - this.lastDrawTime) * this.animationTimeMultiplier);

        this.drawFrame(nextDate);

        // increment currentDate to next date
        this.currentDate = nextDate;

        // update our current progress to match the current date
        this.updateProgressBar();
      }

      this.lastDrawTime = now;

      if (this.highIndex < this.dataLength) {
        window.requestAnimationFrame(this.animate.bind(this));
      } else {
        // we are done
        this.isAnimating = false;
        this.pause();
      }
    };

    CrimeAnimator.prototype.start = function start() {

      // set the current date to the first day if we are at the end of the animation
      if (this.highIndex >= this.dataLength || !this.currentDate) {
        this.currentDate = this.dateRange.start;
      }

      // reset our indexes
      this.lowIndex = 0;
      this.highIndex = 0;

      this.lastDrawTime = new Date().getTime();

      this.isAnimating = true;

      // start the animation
      this.animate();
    };

    CrimeAnimator.prototype.pause = function pause() {

      this.$animationControl.removeClass('pause');
      this.$animationControl.addClass('play');

      this.isPaused = true;

      // draw markers
      if (this.showMarkers) {
        this.addCrimeMarkers();
      }
    };

    CrimeAnimator.prototype.resume = function resume() {

      this.removeCrimeMarkers();

      this.$animationControl.removeClass('play');
      this.$animationControl.addClass('pause');

      this.isPaused = false;

      // if we are at the end of the animation then restart it
      if (!this.isAnimating) {
        this.start();
      }
    };

    CrimeAnimator.prototype.togglePause = function togglePause() {
      if (this.isPaused) {
        this.resume();
      } else {
        this.pause();
      }
    };


    CrimeAnimator.prototype.toggleCrimeMarkers = function toggleCrimeMarkers() {
      this.showMarkers = !this.showMarkers;

      if (!this.showMarkers) {
        this.removeCrimeMarkers();
      } else if (this.isPaused || !this.isAnimating) {
        this.addCrimeMarkers();
      }
    };

    function buildPin(color) {
      return new google.maps.MarkerImage('http:// chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|' + color,
        new google.maps.Size(21, 34),
        new google.maps.Point(0, 0),
        new google.maps.Point(10, 34));
    }

    const crimeTypes = ['Theft from Auto', 'Auto Theft', 'Break & Enter', 'Assault', 'Robbery'];
    const crimePins = [buildPin('BFEADD'), buildPin('A1CDFB'), buildPin('C4B0DD'), buildPin('FAE07E'), buildPin('E5F9A6')];
    CrimeAnimator.prototype.addCrimeMarkers = function addCrimeMarkers() {
      const currentData = this.data.slice(this.lowIndex, this.highIndex + 1);
      const self = this;

      for (let i = currentData.length - 1; i >= 0; i--) {
        const crime = currentData[i];
        const crimeType = crime[2];

        if (this.filters.indexOf(crimeType) >= 0) {
          continue;
        }

        const marker = new google.maps.Marker({
          position: new google.maps.LatLng(crime[0], crime[1]),
          map: this.map,
          icon: crimePins[crimeType],
          title: crimeTypes[crimeType]
        });

        this.markers.push(marker);


      }
    };

    CrimeAnimator.prototype.removeCrimeMarkers = function removeCrimeMarkers() {
      for (let i = this.markers.length - 1; i >= 0; i--) {
        this.markers.pop().setMap(null);
      }
    };

    CrimeAnimator.prototype.getCurrentHeatmapData = function getCurrentHeatmapData(lowIndex, highIndex, visibleThreshold) {
      const currentData = this.data.slice(this.lowIndex, this.highIndex + 1);

      let crime, type, count, timeFromHighIndex;
      const currentTimeRange = this.currentDate - visibleThreshold, heatmapData = [];

      for (let i = currentData.length - 1; i >= 0; i--) {
        crime = currentData[i];
        type = crime[2];

        if (this.filters.indexOf(type) >= 0) {
          continue;
        }

        timeFromHighIndex = this.currentDate - crime[3];
        const x = timeFromHighIndex / currentTimeRange;
        count = Math.abs(4 * (x - Math.pow(x, 2))); // exponential curve for fade in/out
        heatmapData.push({lat: crime[0], lng: crime[1], count: count});
      }

      return {max: 3, data: heatmapData};
    };

    !function ($) {
      $(function () {

        const myLatlng = new google.maps.LatLng(44.6544, -63.5992);

        const myOptions = {
          zoom: 12,
          center: myLatlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDefaultUI: true,
          scrollwheel: true,
          draggable: true,
          navigationControl: true,
          mapTypeControl: false,
          scaleControl: true,
          zoomControl: true,
          zoomControlOptions: {
            style: google.maps.ZoomControlStyle.DEFAULT,
            position: google.maps.ControlPosition.RIGHT_CENTER
          },
          disableDoubleClickZoom: false,
          styles: [{
            'stylers': [
              {'saturation': -26},
              {'lightness': 18},
              {'visibility': 'on'}
            ]
          }]
        };
        const map = new google.maps.Map($('#heatmapArea')[0], myOptions);

        const heatmap = new HeatmapOverlay(map, {
          'radius': 30,
          'visible': true,
          'opacity': 40
        });

        const crimeAnimator = new CrimeAnimator(Data.crimes, heatmap, map);

        window.requestAnimationFrame = window.requestAnimationFrame || window['mozRequestAnimationFrame'] ||
          window.webkitRequestAnimationFrame || window['msRequestAnimationFrame'] ||
          function (fn) {
            return setTimeout(fn, 1000 / 60);
          };

        if (isMobile) {
          $('.mobile-only-content').show();
        }


        // setup the year links and filters drop down
        const yearsMarkup = [];
        for (let i = 0, len = Data.years.length; i < len; i++) {
          const yearz = Data.years[i];
          const isCurrent = Data.year === yearz;
          const isCurrentYear = yearz === new Date().getFullYear();
          yearsMarkup.push('<li><a href="/' + (isCurrentYear ? '' : yearz) + '"' + (isCurrent ? ' class=current' : '') + '>' + yearz + '</a>');
        }
        $('#years').append(yearsMarkup.join(''));

        //  this is important, because if you set the data set too early, the latlng/pixel projection doesn't work
        google.maps.event.addListenerOnce(map, 'idle', function () {
          // wait a bit for the user to get use to the map before we blow their mind with the animation
          window.setTimeout(function () {
            crimeAnimator.start();
          }, 1000);

          // if first launch then display instructions if not mobile
          if (!isMobile && localStorage.getItem('isReturningUser') !== 'true') {
            localStorage.setItem('isReturningUser', 'true');

            // display pointer instructions for 5 seconds after 5 seconds
            const $progressBarInstructions = $('#progress-bar-instructions');
            window.setTimeout(function () {
              $progressBarInstructions.css('display', 'inline-block');
            }, 5000);

            window.setTimeout(function () {
              $progressBarInstructions.hide();
            }, 15000);
          }
        });

      });
    }(window['jQuery']);


    const Data = {
      'years': [2013, 2014, 2015, 2016, 2017, 2018],
      'crimes': [[44.66789, -63.53802, 3, 0, 4], [44.66789, -63.53802, 3, 0, 4], [44.66789, -63.53802, 3, 0, 4], [44.66789, -63.53802, 3, 0, 4], [44.66789, -63.53802, 3, 0, 4], [44.66789, -63.53802, 3, 0, 4], [44.66789, -63.53802, 3, 0, 4], [44.66789, -63.53802, 3, 0, 4], [44.66789, -63.53802, 3, 0, 4], [44.66789, -63.53802, 3, 0, 4], [44.69325, -63.48898, 3, 0, 4], [44.6567, -63.63983, 3, 0, 4], [44.63765, -63.57991, 1, 0, 5], [44.67747, -63.52413, 2, 0, 5], [44.6277, -63.62121, 0, 0, 5], [44.68434, -63.58351, 0, 0, 5], [44.51641, -63.54066, 2, 0, 5], [44.66163, -63.63501, 0, 0, 5], [44.77053, -63.67551, 3, 0, 5], [44.68606, -63.5816, 3, 0, 5], [44.74315, -63.30088, 3, 0, 5], [44.60452, -63.62261, 1, 0, 5], [44.67936, -63.49672, 2, 0, 5], [44.65926, -63.63183, 2, 0, 5], [44.66026, -63.63637, 3, 0, 5], [44.79491, -62.66883, 2, 0, 5], [44.67184, -63.60641, 2, 0, 5], [44.59992, -63.60652, 1, 0, 5], [44.59992, -63.60652, 1, 0, 5], [44.63776, -63.57455, 4, 0, 5], [44.74158, -63.6415, 2, 0, 5], [44.67296, -63.58048, 2, 0, 5], [44.671, -63.58098, 2, 0, 5], [44.78839, -63.72002, 3, 0, 5], [44.6466, -63.59747, 2, 0, 5], [44.64859, -63.61951, 3, 0, 5], [44.6806, -63.55165, 3, 0, 5], [44.65771, -63.65325, 0, 0, 5], [44.65266, -63.75101, 3, 0, 5], [44.67818, -63.58208, 3, 0, 5], [44.67258, -63.57903, 2, 0, 5], [44.90416, -62.46376, 1, 0, 5], [44.67258, -63.57903, 2, 0, 5], [44.67591, -63.6504, 0, 0, 6], [44.67651, -63.56623, 0, 0, 6], [44.66203, -63.66151, 0, 0, 6], [44.66634, -63.56799, 2, 0, 6], [44.65503, -63.62786, 3, 0, 6], [44.80909, -63.58538, 1, 0, 6], [44.66191, -63.63168, 3, 0, 6], [44.66191, -63.63168, 3, 0, 6], [44.67821, -63.58746, 3, 0, 6], [44.64859, -63.61951, 1, 0, 6], [44.73884, -63.26756, 3, 0, 7], [44.73351, -63.88576, 2, 0, 7], [44.72784, -63.80843, 3, 0, 7], [44.79491, -62.66883, 3, 0, 7], [44.72784, -63.80843, 3, 0, 7], [44.75786, -63.85588, 3, 0, 7], [44.67881, -63.65594, 3, 0, 7], [44.68417, -63.49092, 4, 0, 7], [44.65977, -63.64882, 3, 0, 7], [44.65535, -63.65526, 0, 0, 7], [44.65633, -63.58413, 3, 0, 7], [44.59992, -63.60652, 3, 0, 7], [44.64546, -63.58554, 3, 0, 7], [44.76941, -63.21594, 2, 0, 7], [44.64444, -63.57267, 3, 0, 7], [44.69009, -63.53829, 2, 0, 8], [44.82117, -63.73443, 2, 0, 8], [44.6875, -63.59047, 4, 0, 8], [44.67399, -63.5538, 2, 0, 8], [44.66686, -63.617, 1, 0, 8], [44.61957, -63.62082, 3, 0, 8], [44.69639, -63.54533, 0, 0, 8], [44.67763, -63.53858, 2, 0, 8], [44.65414, -63.61156, 1, 0, 8], [44.60406, -63.61277, 2, 0, 9], [44.67947, -63.54028, 3, 0, 9], [44.69985, -63.58668, 2, 0, 9], [44.79975, -63.68682, 2, 0, 9], [44.67998, -63.58713, 3, 0, 9], [44.77874, -63.62305, 0, 0, 9], [44.62577, -63.50746, 3, 0, 9], [44.7721, -63.69375, 3, 0, 9], [44.65639, -63.59996, 4, 0, 9], [44.63822, -63.56982, 4, 0, 9], [44.64834, -63.60398, 0, 0, 9], [44.65931, -63.60887, 3, 0, 9], [44.65933, -63.66691, 3, 0, 9], [44.6862, -63.59745, 3, 0, 9], [44.64072, -63.57519, 3, 0, 9], [44.65409, -63.53528, 3, 0, 9], [44.71974, -63.5858, 3, 0, 9], [44.59856, -63.61481, 3, 0, 10], [44.6373, -63.58534, 4, 0, 10], [44.66729, -63.56953, 2, 0, 10], [44.66624, -63.96067, 3, 0, 10], [44.67984, -63.57962, 0, 0, 10], [44.65406, -63.55156, 3, 0, 10], [44.65289, -63.59245, 4, 0, 10], [44.67212, -63.58034, 2, 0, 10], [44.71871, -63.68406, 3, 0, 10], [44.65263, -63.62945, 3, 0, 10], [44.65651, -63.6212, 2, 0, 10], [44.63263, -63.58892, 0, 0, 10], [44.63822, -63.56982, 3, 0, 10], [44.6559, -63.62291, 3, 0, 10], [44.69082, -63.86818, 2, 0, 10], [44.67041, -63.55082, 2, 0, 10], [44.66733, -63.49449, 2, 0, 11], [44.66759, -63.55214, 3, 0, 11], [44.65286, -63.33859, 2, 0, 11], [44.69857, -63.56738, 3, 0, 11], [44.60568, -63.61645, 3, 0, 11], [44.66223, -63.64151, 3, 0, 11], [44.72754, -63.65081, 2, 0, 11], [44.685, -63.5852, 4, 0, 11], [44.68483, -63.53583, 3, 0, 11], [44.64837, -63.62432, 3, 0, 11], [44.64665, -63.57638, 3, 0, 11], [44.66242, -63.65604, 3, 0, 11], [44.65299, -63.58902, 0, 0, 11], [44.81808, -63.60642, 3, 0, 11], [44.77706, -63.20671, 3, 0, 12], [44.67994, -63.58352, 3, 0, 12], [44.82117, -63.73443, 2, 0, 12], [44.66248, -63.60251, 0, 0, 12], [44.61431, -63.46581, 3, 0, 12], [44.69256, -63.68105, 0, 0, 12], [44.70172, -63.60593, 0, 0, 12], [44.66227, -63.60621, 0, 0, 12], [44.66326, -63.55639, 1, 0, 12], [44.67419, -63.5772, 2, 0, 12], [44.66596, -63.56558, 2, 0, 12], [44.66191, -63.63168, 0, 0, 12], [44.66786, -63.61853, 0, 0, 12], [44.59344, -63.63728, 3, 0, 12], [44.67212, -63.58034, 2, 0, 12], [44.65645, -63.66347, 3, 0, 12], [44.68447, -63.58918, 4, 0, 12], [44.67335, -63.57441, 3, 0, 12], [44.77835, -63.08779, 1, 0, 12], [44.63697, -63.58284, 3, 0, 13], [44.69857, -63.56738, 4, 0, 13], [44.69078, -63.52839, 3, 0, 13], [44.69016, -63.48514, 3, 0, 13], [44.67237, -63.56617, 4, 0, 13], [44.69016, -63.48514, 3, 0, 13], [44.66064, -63.62991, 0, 0, 13], [44.84433, -63.61888, 2, 0, 13], [44.67041, -63.55082, 2, 0, 13], [44.63965, -63.58253, 1, 0, 13], [44.67041, -63.55082, 2, 0, 13], [44.71161, -63.57774, 1, 0, 13], [44.67061, -63.58585, 2, 0, 13], [44.68105, -63.58473, 3, 0, 13], [44.65106, -63.59258, 3, 0, 13], [44.65348, -63.63945, 0, 0, 13], [44.67041, -63.55082, 2, 0, 13], [44.67041, -63.55082, 2, 0, 13], [44.60161, -63.61262, 4, 0, 14], [44.74042, -63.55677, 2, 0, 14], [44.64738, -63.57566, 3, 0, 14], [44.66524, -63.61403, 3, 0, 14], [44.65071, -63.59292, 3, 0, 14], [44.67661, -63.53611, 3, 0, 14], [44.64478, -63.57573, 2, 0, 14], [44.64191, -63.57066, 3, 0, 14], [44.579, -63.57872, 3, 0, 14], [44.80096, -63.67306, 2, 0, 14], [44.73429, -63.39323, 1, 0, 14], [44.63742, -64.05032, 3, 0, 14], [44.6818, -63.58632, 3, 0, 14], [44.67221, -63.55985, 1, 0, 15], [44.66438, -63.60939, 3, 0, 15], [44.58358, -63.5837, 3, 0, 15], [44.71903, -63.58062, 2, 0, 15], [44.66308, -63.65439, 3, 0, 15], [44.63647, -63.57698, 3, 0, 15], [44.69833, -63.48973, 1, 0, 15], [44.79126, -63.71883, 3, 0, 15], [44.78051, -63.35723, 2, 0, 15], [44.65154, -63.61212, 0, 0, 15], [44.77874, -63.62305, 0, 0, 15], [44.65088, -63.58169, 0, 0, 15], [44.62581, -63.5744, 0, 0, 15], [44.65578, -63.54989, 0, 0, 15], [44.67212, -63.58034, 2, 0, 15], [44.74854, -63.29788, 2, 0, 15], [44.6721, -63.58376, 3, 0, 16], [44.67269, -63.5456, 1, 0, 16], [44.6449, -63.59769, 3, 0, 16], [44.69643, -63.60218, 0, 0, 16], [44.81208, -62.88641, 2, 0, 16], [44.84805, -62.68432, 2, 0, 16], [44.71974, -63.5858, 3, 0, 16], [44.6455, -63.59119, 2, 0, 16], [44.63618, -63.59886, 3, 0, 16], [44.66079, -63.66294, 0, 0, 16], [44.68667, -63.57091, 3, 0, 16], [44.72359, -63.565, 1, 0, 17], [44.69122, -63.5367, 3, 0, 17], [44.60433, -63.61543, 0, 0, 17], [44.87101, -63.54117, 0, 0, 17], [44.72359, -63.565, 2, 0, 17], [44.66529, -63.61044, 4, 0, 17], [44.68298, -63.58624, 3, 0, 17], [44.69985, -63.58668, 2, 0, 17], [44.64457, -63.59075, 3, 0, 17], [44.69078, -63.52839, 3, 0, 17], [44.63877, -63.58591, 2, 0, 17], [44.60537, -63.6091, 0, 0, 17], [44.6734, -63.57826, 2, 0, 17], [44.65547, -63.59082, 0, 0, 17], [44.67059, -63.67432, 3, 0, 17], [44.59344, -63.63728, 3, 0, 17], [44.6721, -63.58376, 3, 0, 17], [44.71974, -63.5858, 3, 0, 18], [44.6541, -63.58643, 2, 0, 18], [44.65578, -63.54989, 3, 0, 18], [44.70228, -63.66146, 2, 0, 18], [44.6603, -63.59544, 4, 0, 18], [44.67061, -63.58585, 2, 0, 18], [44.66163, -63.63501, 0, 0, 18], [44.6646, -63.5678, 3, 0, 18], [44.65508, -63.62171, 3, 0, 18], [44.70114, -63.53655, 2, 0, 18], [44.67681, -63.55637, 2, 0, 18], [44.67681, -63.55637, 0, 0, 18], [44.68627, -63.56043, 3, 0, 18], [44.6541, -63.58643, 2, 0, 18], [44.7675, -63.69425, 3, 0, 18], [44.65062, -63.42472, 2, 0, 18], [44.81182, -63.76173, 3, 0, 22], [44.71313, -63.67876, 3, 0, 22], [44.68681, -63.58765, 3, 0, 22], [44.6746, -63.61174, 3, 0, 22], [44.76543, -63.68016, 4, 0, 22], [44.67981, -63.58823, 3, 0, 22], [44.65046, -63.65429, 1, 0, 22], [44.6699, -63.563, 2, 0, 22], [44.62525, -63.59329, 2, 0, 22], [44.86608, -63.62367, 0, 0, 22], [44.54368, -63.71957, 1, 0, 22], [44.59992, -63.60652, 3, 0, 22], [44.76707, -63.6852, 0, 0, 22], [44.65004, -63.58054, 3, 0, 22], [44.67374, -63.57708, 3, 0, 23], [44.67041, -63.55082, 2, 0, 23], [44.67706, -63.64987, 3, 0, 23], [44.66477, -63.53804, 3, 0, 23], [44.73385, -63.65545, 0, 0, 23], [44.76609, -63.68323, 3, 0, 23], [44.66369, -63.67069, 3, 0, 23], [44.65578, -63.54989, 3, 0, 23], [44.70119, -63.6885, 0, 0, 23], [44.6776, -63.58495, 0, 0, 23], [44.67041, -63.55082, 2, 0, 23], [44.6717, -63.65813, 0, 0, 23], [44.69737, -63.5476, 4, 0, 23], [44.67041, -63.55082, 2, 0, 23], [44.65128, -63.67358, 0, 0, 23], [44.67399, -63.5538, 2, 0, 24], [44.65633, -63.58413, 3, 0, 24], [44.72005, -63.70401, 3, 0, 24], [44.86951, -63.53757, 1, 0, 24], [44.64369, -63.60045, 0, 0, 24], [44.68313, -63.58486, 3, 0, 24], [44.72005, -63.70401, 2, 0, 24], [44.59972, -63.61401, 3, 0, 24], [44.59896, -63.61462, 3, 0, 24], [44.69588, -63.45877, 1, 0, 24], [44.6469, -63.46279, 4, 0, 24], [44.66163, -63.63501, 3, 0, 24], [44.65239, -63.59517, 0, 0, 24], [44.6771, -63.58416, 0, 0, 24], [44.60101, -63.61316, 3, 0, 24], [44.65618, -63.59774, 0, 0, 25], [44.702, -63.47551, 3, 0, 25], [44.64576, -63.57333, 2, 0, 25], [44.67568, -63.59162, 3, 0, 25], [44.77979, -63.69518, 3, 0, 25], [44.72028, -63.54478, 2, 0, 25], [44.68447, -63.58918, 3, 0, 25], [44.64337, -63.62853, 0, 0, 25], [44.77201, -63.69851, 3, 0, 25], [44.69023, -63.59978, 0, 0, 25], [44.63882, -63.58737, 0, 0, 25], [44.64518, -63.62153, 3, 0, 25], [44.64358, -63.57224, 3, 0, 26], [44.67251, -63.49776, 3, 0, 26], [44.85221, -63.68307, 0, 0, 26], [44.66651, -63.56561, 3, 0, 26], [44.63903, -63.66684, 0, 0, 26], [44.67041, -63.57461, 3, 0, 26], [44.71974, -63.5858, 3, 0, 26], [44.67184, -63.57481, 4, 0, 26], [44.68797, -63.52787, 3, 0, 26], [44.7684, -63.66272, 0, 0, 27], [44.63529, -63.68053, 1, 0, 27], [44.80349, -63.68299, 3, 0, 27], [44.6851, -63.58969, 3, 0, 27], [44.66634, -63.56799, 3, 0, 27], [44.65346, -63.58411, 2, 0, 27], [44.65004, -63.58054, 3, 0, 27], [44.67914, -63.59409, 3, 0, 27], [44.66975, -63.56658, 0, 0, 27], [44.76164, -63.65766, 0, 0, 27], [44.63747, -63.62029, 3, 0, 27], [44.66975, -63.56658, 2, 0, 27], [44.71974, -63.5858, 3, 0, 28], [44.64615, -63.57393, 3, 0, 28], [44.6596, -63.5957, 2, 0, 28], [44.65122, -63.58308, 4, 0, 28], [44.78024, -63.71127, 0, 0, 28], [44.68105, -63.58473, 2, 0, 28], [44.67466, -63.64708, 3, 0, 28], [44.68174, -63.50843, 3, 0, 28], [44.68849, -63.58332, 1, 0, 28], [44.54098, -63.63873, 3, 0, 28], [44.68074, -63.65731, 2, 0, 28], [44.67209, -63.57895, 3, 0, 28], [44.65411, -63.65408, 0, 0, 28], [44.66399, -63.63973, 0, 0, 28], [44.66459, -63.56859, 3, 0, 28], [44.66117, -63.49328, 3, 0, 28], [44.76342, -63.63913, 2, 0, 29], [44.64065, -63.58138, 3, 0, 29], [44.6569, -63.65663, 3, 0, 29], [44.63997, -63.69446, 1, 0, 29], [44.78369, -63.69365, 0, 0, 29], [44.65979, -63.61547, 3, 0, 29], [44.63776, -63.57455, 3, 0, 29], [44.65114, -63.64202, 3, 0, 29], [44.68434, -63.58351, 3, 0, 29], [44.67568, -63.59162, 3, 0, 29], [44.63377, -63.57849, 3, 0, 29], [44.6547, -63.59265, 0, 0, 29], [44.66634, -63.56799, 3, 0, 29], [44.67041, -63.55082, 2, 0, 29], [44.67007, -63.67118, 3, 0, 29], [44.71974, -63.5858, 3, 0, 29], [44.59896, -63.61462, 3, 0, 29], [44.6569, -63.65663, 3, 0, 29], [44.68608, -63.52203, 3, 0, 29], [44.66377, -63.60564, 2, 0, 29], [44.66533, -63.49997, 0, 0, 29], [44.9696, -62.25915, 2, 0, 29], [44.72501, -63.73801, 3, 0, 30], [44.67087, -63.66559, 0, 0, 30], [44.65261, -63.59345, 2, 0, 30], [44.7675, -63.69425, 4, 0, 30], [44.65337, -63.58062, 3, 0, 30], [44.65578, -63.54989, 3, 0, 30], [44.66594, -63.53914, 3, 0, 30], [44.7633, -63.70024, 3, 0, 30], [44.67041, -63.57461, 3, 0, 30], [44.77533, -63.67371, 3, 0, 31], [44.74982, -63.66071, 3, 0, 31], [44.6661, -63.50629, 3, 0, 31], [44.71974, -63.5858, 3, 0, 31], [44.77946, -63.67346, 3, 0, 31], [44.63165, -63.58277, 3, 0, 31], [44.92049, -62.53494, 2, 0, 31], [44.72199, -63.64813, 3, 0, 31], [44.62826, -63.5713, 4, 0, 31], [44.6521, -63.58436, 3, 0, 31], [44.69643, -63.60218, 3, 0, 31], [44.69985, -63.58668, 2, 0, 31], [44.67316, -63.58439, 3, 0, 31], [44.60853, -63.48183, 3, 0, 31], [44.64358, -63.57224, 3, 0, 31], [44.56833, -63.56669, 3, 0, 31], [44.65342, -63.59077, 0, 1, 1], [44.71974, -63.5858, 3, 1, 1], [44.67192, -63.58793, 0, 1, 1], [44.65792, -63.62923, 3, 1, 1], [44.66333, -63.60247, 3, 1, 1], [45.05379, -63.12645, 2, 1, 1], [44.64603, -63.59506, 4, 1, 1], [44.64359, -63.5772, 4, 1, 1], [44.67408, -63.66978, 0, 1, 1], [44.6548, -63.7414, 2, 1, 1], [44.63697, -63.58284, 3, 1, 1], [44.66463, -63.65339, 0, 1, 1], [44.68383, -63.57147, 0, 1, 1], [44.67437, -63.66681, 0, 1, 1], [44.65918, -63.63526, 3, 1, 1], [44.61112, -63.48078, 2, 1, 2], [44.81075, -63.6084, 1, 1, 2], [44.56324, -63.56535, 0, 1, 2], [44.53403, -63.65223, 1, 1, 2], [44.6364, -63.6793, 3, 1, 2], [44.6705, -63.51406, 3, 1, 2], [44.68884, -63.53222, 2, 1, 2], [44.68291, -63.58753, 3, 1, 2], [44.63967, -63.57167, 3, 1, 2], [44.75792, -63.6604, 3, 1, 2], [44.70681, -63.55143, 4, 1, 2], [44.665, -63.74975, 3, 1, 2], [44.71974, -63.5858, 3, 1, 2], [44.66998, -63.60824, 3, 1, 2], [44.63752, -63.59247, 3, 1, 2], [44.67031, -63.56843, 3, 1, 2], [44.70794, -63.66599, 1, 1, 2], [44.59406, -63.60047, 3, 1, 2], [44.64008, -63.59045, 0, 1, 3], [44.77411, -63.68999, 3, 1, 3], [44.6562, -63.58593, 3, 1, 3], [44.67393, -63.58406, 1, 1, 3], [44.75557, -63.66177, 2, 1, 3], [44.65811, -63.28278, 3, 1, 3], [44.70969, -63.56776, 0, 1, 3], [44.70911, -63.66745, 2, 1, 3], [44.65645, -63.62812, 3, 1, 3], [44.66666, -63.56498, 3, 1, 3], [44.68187, -63.54015, 4, 1, 4], [44.66323, -63.61583, 2, 1, 4], [44.69985, -63.58668, 2, 1, 4], [44.72887, -63.67069, 3, 1, 4], [44.67911, -63.55143, 4, 1, 4], [44.64501, -63.57412, 3, 1, 4], [44.82416, -63.77658, 3, 1, 4], [44.65725, -63.62117, 3, 1, 4], [44.65725, -63.62117, 3, 1, 4], [44.67114, -63.5306, 4, 1, 4], [44.65596, -63.73472, 2, 1, 4], [44.65248, -63.58825, 0, 1, 5], [44.68568, -63.58629, 3, 1, 5], [44.89211, -62.41958, 2, 1, 5], [44.64358, -63.57224, 3, 1, 5], [44.71811, -63.67467, 3, 1, 5], [44.67041, -63.55082, 3, 1, 5], [44.59688, -63.60241, 3, 1, 5], [44.65277, -63.58524, 0, 1, 5], [44.68023, -63.65848, 0, 1, 5], [44.70904, -63.5912, 0, 1, 5], [44.59992, -63.60652, 3, 1, 6], [44.85662, -63.60069, 0, 1, 6], [44.91997, -62.52068, 2, 1, 6], [44.91152, -62.51597, 2, 1, 6], [44.69626, -63.48876, 1, 1, 6], [44.98419, -62.6749, 2, 1, 6], [44.98419, -62.6749, 2, 1, 6], [44.77533, -63.67371, 0, 1, 6], [44.60804, -63.61364, 3, 1, 6], [44.64877, -63.59923, 0, 1, 6], [44.85662, -63.60069, 0, 1, 6], [44.62647, -63.59741, 0, 1, 6], [44.68568, -63.58629, 3, 1, 6], [44.65618, -63.59774, 0, 1, 6], [44.64735, -63.59934, 0, 1, 7], [44.63025, -63.62179, 3, 1, 7], [44.64705, -63.59101, 3, 1, 7], [44.66522, -63.61496, 3, 1, 7], [44.6725, -63.61493, 2, 1, 7], [44.91906, -62.5181, 3, 1, 7], [44.6539, -63.54591, 3, 1, 7], [44.68105, -63.58473, 3, 1, 7], [44.72777, -63.67148, 3, 1, 7], [44.685, -63.5852, 3, 1, 7], [44.78318, -63.67975, 2, 1, 8], [44.66785, -63.56251, 3, 1, 8], [44.67339, -63.52929, 2, 1, 8], [44.84477, -63.66741, 3, 1, 8], [44.68915, -63.58684, 3, 1, 8], [44.72876, -63.68177, 1, 1, 9], [44.62874, -63.52029, 3, 1, 9], [44.78755, -63.64706, 0, 1, 9], [44.5932, -63.91705, 2, 1, 9], [44.68002, -63.21368, 4, 1, 9], [44.68122, -63.66047, 3, 1, 9], [44.72832, -63.64699, 3, 1, 9], [44.64372, -63.57437, 4, 1, 9], [44.79587, -63.65661, 2, 1, 10], [44.88639, -63.51536, 3, 1, 10], [44.70119, -63.6885, 1, 1, 10], [44.73094, -63.67085, 0, 1, 10], [44.68122, -63.66047, 2, 1, 10], [44.67109, -63.53317, 0, 1, 10], [44.64814, -63.62064, 3, 1, 10], [44.69857, -63.56738, 3, 1, 10], [44.65241, -63.62049, 3, 1, 10], [44.59617, -63.55653, 2, 1, 10], [44.73101, -63.67475, 0, 1, 10], [44.64398, -63.67471, 2, 1, 10], [44.68343, -63.50446, 0, 1, 10], [44.65842, -63.49848, 3, 1, 10], [44.65205, -63.58733, 0, 1, 10], [44.72904, -63.6752, 0, 1, 10], [44.67445, -63.59015, 3, 1, 10], [44.74982, -63.66071, 3, 1, 10], [44.64546, -63.59284, 0, 1, 10], [44.68886, -63.68804, 3, 1, 10], [44.60315, -63.60908, 0, 1, 10], [44.68501, -63.58034, 3, 1, 11], [44.65927, -63.6339, 3, 1, 11], [44.64656, -63.57454, 3, 1, 11], [44.6547, -63.59265, 3, 1, 11], [44.65021, -63.60048, 0, 1, 11], [44.66716, -63.49756, 0, 1, 11], [44.68429, -63.49972, 0, 1, 11], [44.69071, -63.54633, 0, 1, 11], [44.6541, -63.58643, 2, 1, 11], [44.65633, -63.58413, 3, 1, 11], [44.68869, -63.58051, 3, 1, 11], [44.48733, -63.60881, 1, 1, 11], [44.65054, -63.61071, 0, 1, 12], [44.6514, -63.58212, 3, 1, 12], [44.64926, -63.59163, 3, 1, 12], [44.78949, -63.69002, 3, 1, 12], [44.78861, -63.70153, 3, 1, 12], [44.66266, -63.5551, 3, 1, 12], [44.67374, -63.4972, 3, 1, 12], [44.64175, -63.58765, 3, 1, 12], [44.66842, -63.67539, 0, 1, 12], [44.65146, -63.60589, 0, 1, 12], [44.727, -63.71031, 3, 1, 12], [44.66163, -63.63501, 0, 1, 12], [44.61893, -63.46526, 3, 1, 12], [44.77211, -63.69376, 2, 1, 12], [44.69036, -63.59145, 0, 1, 12], [44.72622, -63.64318, 2, 1, 12], [44.72053, -63.56233, 1, 1, 12], [45.0036, -63.36792, 2, 1, 12], [44.68483, -63.53583, 3, 1, 12], [44.68681, -63.58765, 1, 1, 12], [44.72211, -63.671, 2, 1, 12], [44.65337, -63.58062, 3, 1, 13], [44.66152, -63.94521, 2, 1, 13], [44.64444, -63.57267, 0, 1, 13], [44.63214, -63.66198, 0, 1, 13], [44.69122, -63.5367, 3, 1, 13], [44.77699, -63.60274, 2, 1, 13], [44.64705, -63.57255, 2, 1, 13], [44.67212, -63.58034, 2, 1, 14], [44.69818, -63.54987, 3, 1, 14], [44.69364, -63.4962, 3, 1, 14], [44.67212, -63.58034, 2, 1, 14], [44.65331, -63.59184, 0, 1, 14], [44.65698, -63.63921, 3, 1, 14], [44.66619, -63.5704, 2, 1, 14], [44.72934, -63.67221, 3, 1, 14], [44.66079, -63.66294, 0, 1, 14], [44.65633, -63.58413, 3, 1, 14], [44.64546, -63.58554, 3, 1, 14], [44.63967, -63.57167, 3, 1, 15], [44.65257, -63.61793, 4, 1, 15], [44.68864, -63.60753, 3, 1, 15], [44.67199, -63.52397, 3, 1, 15], [44.78727, -63.69438, 3, 1, 15], [44.65671, -63.6337, 3, 1, 15], [44.66353, -63.61271, 0, 1, 16], [44.60468, -63.61089, 3, 1, 16], [44.67596, -63.50749, 3, 1, 16], [44.64724, -63.65408, 0, 1, 16], [44.65004, -63.60998, 0, 1, 16], [44.71579, -63.67214, 3, 1, 16], [44.7687, -63.68677, 3, 1, 16], [44.65264, -63.62948, 3, 1, 16], [44.6562, -63.58593, 3, 1, 16], [44.64691, -63.59363, 3, 1, 16], [44.77828, -63.67011, 3, 1, 17], [44.60468, -63.61089, 3, 1, 17], [44.64484, -63.61191, 0, 1, 17], [44.67808, -63.59112, 0, 1, 17], [44.61429, -63.6183, 0, 1, 17], [44.68681, -63.58765, 0, 1, 17], [44.70638, -63.50098, 3, 1, 17], [44.64082, -63.56864, 3, 1, 17], [44.64358, -63.57224, 3, 1, 17], [44.76103, -63.65881, 3, 1, 17], [44.62631, -63.5769, 0, 1, 17], [44.7408, -63.30039, 2, 1, 18], [44.77289, -63.58098, 0, 1, 18], [44.67129, -63.49641, 3, 1, 18], [44.65992, -63.63329, 3, 1, 18], [44.73385, -63.65545, 0, 1, 18], [44.76512, -63.57326, 0, 1, 18], [44.71147, -63.68746, 3, 1, 18], [44.71974, -63.5858, 3, 1, 18], [44.74004, -63.54467, 0, 1, 18], [44.64849, -63.65593, 3, 1, 18], [44.6277, -63.62121, 2, 1, 18], [44.82182, -63.61639, 0, 1, 18], [44.74225, -63.54144, 0, 1, 18], [44.75349, -63.64674, 0, 1, 18], [44.6273, -63.6275, 3, 1, 19], [44.7481, -63.62764, 2, 1, 19], [44.65395, -63.66386, 2, 1, 19], [44.65637, -63.63538, 3, 1, 19], [44.86425, -63.54248, 0, 1, 19], [44.64656, -63.57454, 3, 1, 19], [44.65671, -63.59024, 2, 1, 19], [44.64738, -63.57566, 3, 1, 19], [44.80704, -63.62771, 0, 1, 19], [44.71974, -63.5858, 3, 1, 19], [44.65405, -63.60441, 0, 1, 19], [44.72933, -63.41702, 3, 1, 19], [44.68915, -63.58684, 1, 1, 19], [44.69807, -63.58296, 4, 1, 20], [44.65181, -63.54746, 3, 1, 20], [44.74427, -63.79649, 0, 1, 20], [44.6639, -63.66038, 3, 1, 20], [44.66014, -63.65624, 3, 1, 20], [44.61511, -63.48497, 1, 1, 20], [44.68864, -63.60753, 3, 1, 20], [44.74353, -63.80494, 3, 1, 20], [44.68359, -63.58191, 0, 1, 20], [44.6818, -63.56806, 0, 1, 20], [44.63902, -63.57817, 0, 1, 20], [44.6728, -63.49404, 3, 1, 20], [44.62945, -63.57533, 3, 1, 20], [44.6569, -63.65661, 0, 1, 20], [44.67922, -63.49916, 0, 1, 20], [44.71974, -63.5858, 3, 1, 20], [44.64446, -63.57, 2, 1, 20], [44.60947, -63.4709, 3, 1, 20], [44.69936, -63.59193, 3, 1, 21], [44.6061, -63.61217, 2, 1, 21], [44.65899, -63.5986, 0, 1, 21], [44.79589, -63.62295, 3, 1, 21], [44.64966, -63.59861, 0, 1, 21], [44.68045, -63.55026, 3, 1, 21], [44.6817, -63.66444, 3, 1, 21], [44.72549, -63.66935, 3, 1, 21], [44.62922, -63.62677, 0, 1, 21], [44.63872, -63.62716, 0, 1, 21], [44.63824, -63.6288, 0, 1, 21], [44.63082, -63.6266, 0, 1, 21], [44.65816, -63.58705, 2, 1, 21], [44.60385, -63.61097, 4, 1, 21], [44.70548, -63.56765, 0, 1, 21], [44.66724, -63.59929, 3, 1, 21], [44.65633, -63.58413, 0, 1, 21], [44.65134, -63.59959, 0, 1, 22], [44.66227, -63.65994, 2, 1, 22], [44.6527, -63.5768, 3, 1, 22], [44.63341, -63.57546, 0, 1, 22], [44.78071, -63.71108, 3, 1, 22], [44.63872, -63.62716, 0, 1, 22], [44.66818, -63.56087, 2, 1, 22], [44.82472, -63.68439, 3, 1, 22], [44.77694, -63.20961, 3, 1, 23], [44.63341, -63.62576, 3, 1, 23], [44.65049, -63.59387, 4, 1, 23], [44.6503, -63.58395, 0, 1, 23], [44.6607, -63.63558, 1, 1, 23], [44.68988, -63.52925, 3, 1, 23], [44.77706, -63.20671, 3, 1, 23], [44.64358, -63.57224, 3, 1, 23], [44.66186, -63.59898, 2, 1, 23], [44.87101, -63.54117, 0, 1, 23], [44.78266, -63.61573, 2, 1, 23], [44.65456, -63.58568, 3, 1, 23], [44.6742, -63.57657, 3, 1, 23], [44.64885, -63.57418, 2, 1, 23], [44.65671, -63.59024, 3, 1, 23], [44.64242, -63.57925, 3, 1, 23], [44.65671, -63.59024, 0, 1, 24], [44.67942, -63.58955, 3, 1, 24], [44.88639, -63.51536, 3, 1, 24], [44.7547, -63.66318, 3, 1, 24], [44.73413, -63.29081, 3, 1, 24], [44.69167, -63.58368, 3, 1, 24], [44.68003, -63.58273, 0, 1, 24], [44.67041, -63.57461, 3, 1, 24], [44.65088, -63.58169, 3, 1, 24], [44.67872, -63.52169, 3, 1, 24], [44.67176, -63.58907, 3, 1, 24], [44.67086, -63.60653, 1, 1, 24], [44.67228, -63.67028, 3, 1, 24], [44.6731, -63.4884, 3, 1, 24], [44.81636, -63.61147, 3, 1, 24], [44.67228, -63.67028, 3, 1, 24], [44.63502, -63.59158, 0, 1, 24], [44.71974, -63.5858, 3, 1, 24], [44.67159, -63.66377, 0, 1, 24], [44.63428, -63.5721, 3, 1, 24], [44.65986, -63.6742, 0, 1, 24], [44.64968, -63.66814, 0, 1, 25], [44.77364, -63.70114, 0, 1, 25], [44.68237, -63.58326, 3, 1, 25], [44.65901, -63.65792, 2, 1, 25], [44.65337, -63.58062, 3, 1, 25], [44.77738, -63.67227, 0, 1, 25], [44.67251, -63.49776, 0, 1, 25], [44.6562, -63.58593, 3, 1, 25], [44.78201, -63.70241, 3, 1, 25], [44.63496, -63.57076, 3, 1, 25], [44.73439, -63.38796, 2, 1, 25], [44.68483, -63.53583, 3, 1, 25], [44.63629, -63.66916, 0, 1, 25], [44.76691, -63.68959, 1, 1, 26], [44.6392, -63.6745, 2, 1, 26], [44.6541, -63.58643, 2, 1, 26], [44.68483, -63.53583, 3, 1, 26], [44.72439, -63.57512, 2, 1, 26], [44.60531, -63.61314, 1, 1, 26], [44.65681, -63.59666, 0, 1, 26], [44.69741, -63.68871, 0, 1, 26], [44.66171, -63.61421, 3, 1, 26], [44.67237, -63.56617, 0, 1, 26], [44.62457, -63.51215, 2, 1, 26], [44.64334, -63.57814, 3, 1, 26], [44.69503, -63.6008, 0, 1, 26], [44.66785, -63.56251, 4, 1, 26], [44.60278, -63.6105, 3, 1, 26], [44.64978, -63.57835, 3, 1, 26], [44.66194, -63.63296, 0, 1, 26], [44.67228, -63.61183, 3, 1, 26], [44.65037, -63.58174, 1, 1, 27], [44.64733, -63.59071, 3, 1, 27], [44.65261, -63.59345, 0, 1, 27], [44.74396, -63.28302, 3, 1, 27], [44.64542, -63.66814, 1, 1, 27], [44.73439, -63.38796, 2, 1, 27], [44.68417, -63.49092, 3, 1, 27], [44.77219, -63.65646, 2, 1, 27], [44.64579, -63.67018, 0, 1, 27], [44.60482, -63.46969, 3, 1, 27], [44.68411, -63.58497, 3, 1, 27], [44.77809, -63.67239, 0, 1, 27], [44.65162, -63.5859, 0, 1, 27], [44.65651, -63.58687, 3, 1, 27], [44.70183, -63.7553, 2, 1, 27], [44.68627, -63.56043, 0, 1, 27], [44.64579, -63.67018, 0, 1, 27], [44.81075, -63.6084, 0, 1, 27], [44.63086, -63.57771, 3, 1, 27], [44.67108, -63.61202, 0, 1, 27], [44.71903, -63.58062, 0, 1, 27], [44.68903, -63.54206, 3, 1, 27], [44.6427, -63.57034, 0, 1, 28], [44.64721, -63.57193, 0, 1, 28], [44.70725, -63.67678, 3, 1, 28], [44.67372, -63.48616, 0, 1, 28], [44.79404, -63.15633, 2, 1, 28], [44.6362, -63.66937, 0, 1, 28], [44.6639, -63.66038, 2, 1, 28], [44.6514, -63.58212, 3, 1, 28], [44.78307, -63.69785, 2, 1, 28], [44.64778, -63.57623, 3, 1, 28], [44.65827, -63.64185, 0, 1, 28], [44.67372, -63.48616, 0, 1, 28], [44.68681, -63.58765, 3, 1, 28], [44.65233, -63.58917, 0, 1, 28], [44.65348, -63.58859, 0, 1, 28], [44.6519, -63.58323, 3, 1, 28], [44.64615, -63.59157, 0, 1, 28], [44.67372, -63.48616, 0, 1, 28], [44.67114, -63.5306, 3, 1, 28], [44.66847, -63.66308, 2, 1, 28], [44.93109, -63.54244, 3, 2, 1], [44.65261, -63.59345, 3, 2, 1], [44.78524, -63.67608, 3, 2, 1], [44.91669, -63.55795, 2, 2, 1], [44.59992, -63.60652, 0, 2, 1], [44.6733, -63.57757, 3, 2, 1], [44.64518, -63.62154, 3, 2, 1], [44.69296, -63.50303, 3, 2, 1], [44.66962, -63.56865, 3, 2, 1], [44.64716, -63.73599, 3, 2, 1], [44.66027, -63.63639, 0, 2, 1], [44.65491, -63.5907, 0, 2, 1], [44.65337, -63.58062, 3, 2, 1], [44.64873, -63.64749, 0, 2, 1], [44.66027, -63.63639, 0, 2, 1], [44.63903, -63.66684, 2, 2, 1], [44.60779, -63.47032, 3, 2, 2], [44.66222, -63.64151, 2, 2, 2], [44.71035, -63.68848, 2, 2, 2], [44.63165, -63.58277, 2, 2, 2], [44.71974, -63.5858, 3, 2, 2], [44.66368, -63.26, 2, 2, 2], [44.66009, -63.63547, 2, 2, 2], [44.71974, -63.5858, 3, 2, 2], [44.72211, -63.671, 2, 2, 2], [44.68681, -63.58765, 3, 2, 2], [44.76902, -63.6512, 1, 2, 2], [44.67258, -63.57903, 3, 2, 2], [44.61156, -63.46379, 3, 2, 2], [44.68849, -63.58332, 3, 2, 2], [44.63963, -63.59386, 0, 2, 2], [44.65282, -63.59053, 3, 2, 2], [44.70337, -63.68543, 0, 2, 2], [44.68417, -63.49092, 3, 2, 2], [44.66764, -63.641, 0, 2, 2], [44.60377, -63.61861, 3, 2, 2], [44.66335, -63.66938, 2, 2, 2], [44.68261, -63.53628, 4, 2, 3], [44.66194, -63.66239, 2, 2, 3], [44.76846, -62.95734, 1, 2, 3], [44.78246, -63.75346, 2, 2, 3], [44.65413, -63.63342, 3, 2, 3], [44.64198, -63.57584, 4, 2, 3], [44.66023, -63.6533, 2, 2, 3], [44.64309, -63.59013, 0, 2, 3], [44.69618, -63.66265, 3, 2, 3], [44.66976, -63.4909, 0, 2, 3], [44.69493, -63.67853, 2, 2, 3], [44.66199, -63.27452, 3, 2, 3], [44.6875, -63.59047, 3, 2, 3], [44.68313, -63.58486, 2, 2, 3], [44.6458, -63.57191, 3, 2, 3], [44.65645, -63.66348, 0, 2, 3], [44.66242, -63.65604, 0, 2, 3], [44.64738, -63.57566, 2, 2, 3], [44.64665, -63.57638, 3, 2, 3], [44.72359, -63.565, 3, 2, 3], [44.80987, -63.60139, 3, 2, 3], [44.65762, -63.64196, 3, 2, 3], [44.64981, -63.72321, 3, 2, 3], [44.64303, -63.6106, 0, 2, 3], [44.60268, -63.61473, 0, 2, 3], [44.69063, -63.53228, 4, 2, 3], [44.71974, -63.5858, 3, 2, 4], [44.69737, -63.5476, 4, 2, 4], [44.63518, -63.58023, 1, 2, 4], [44.65818, -63.63935, 2, 2, 4], [44.61893, -63.46526, 3, 2, 4], [44.63737, -63.57884, 4, 2, 4], [44.645, -63.57293, 3, 2, 4], [44.66023, -63.6533, 2, 2, 4], [44.74514, -63.30209, 3, 2, 4], [44.66976, -63.4909, 0, 2, 4], [44.66512, -63.64632, 0, 2, 4], [44.67822, -63.52048, 3, 2, 4], [44.6413, -63.63467, 0, 2, 4], [44.66073, -63.62051, 3, 2, 4], [44.68627, -63.56043, 0, 2, 4], [44.65762, -63.64196, 3, 2, 4], [44.67514, -63.48512, 0, 2, 4], [44.68288, -63.4812, 2, 2, 4], [44.64503, -63.5741, 3, 2, 4], [44.64359, -63.57223, 3, 2, 4], [44.64479, -63.57246, 3, 2, 4], [44.65372, -63.66665, 2, 2, 4], [44.68401, -63.67407, 3, 2, 4], [44.67033, -63.49588, 0, 2, 4], [44.65337, -63.58062, 3, 2, 4], [44.67033, -63.49588, 0, 2, 4], [44.66747, -63.92979, 1, 2, 4], [44.71974, -63.5858, 3, 2, 5], [44.59968, -63.61172, 3, 2, 5], [44.65926, -63.63182, 4, 2, 5], [44.71615, -63.60086, 2, 2, 5], [44.68087, -63.4903, 0, 2, 5], [44.78071, -63.69984, 3, 2, 5], [44.68447, -63.58918, 3, 2, 5], [44.68726, -63.59928, 1, 2, 5], [44.68705, -63.58338, 3, 2, 5], [44.6592, -63.55242, 0, 2, 5], [44.65923, -63.65861, 0, 2, 5], [44.64952, -63.62791, 0, 2, 5], [44.64194, -63.63094, 0, 2, 5], [44.64445, -63.57266, 3, 2, 6], [44.71203, -63.68241, 0, 2, 6], [44.47358, -63.72217, 0, 2, 6], [44.70841, -63.56051, 3, 2, 6], [44.63203, -63.62246, 2, 2, 6], [44.69078, -63.52839, 0, 2, 6], [44.66079, -63.66294, 3, 2, 6], [44.67844, -63.55415, 2, 2, 6], [44.66152, -63.67141, 2, 2, 6], [44.69023, -63.59978, 1, 2, 6], [44.65613, -63.65104, 3, 2, 6], [44.68681, -63.58765, 3, 2, 6], [45.03291, -63.26811, 2, 2, 6], [44.80987, -63.60139, 3, 2, 6], [44.7729, -63.68638, 3, 2, 6], [44.67844, -63.55415, 1, 2, 6], [44.65148, -63.58865, 0, 2, 6], [44.7729, -63.68638, 3, 2, 6], [44.67053, -63.65479, 0, 2, 7], [44.68447, -63.58918, 3, 2, 7], [44.64144, -63.68524, 2, 2, 7], [44.68084, -63.53903, 0, 2, 7], [44.65965, -63.60428, 3, 2, 7], [44.64966, -63.59861, 0, 2, 7], [44.68441, -63.42366, 3, 2, 7], [44.68597, -63.54467, 3, 2, 7], [44.59688, -63.60241, 3, 2, 7], [44.77963, -63.67905, 3, 2, 7], [44.65732, -63.64035, 0, 2, 7], [44.62506, -63.61912, 1, 2, 7], [44.70612, -63.6641, 2, 2, 7], [44.64296, -63.59875, 3, 2, 7], [44.74282, -63.38126, 2, 2, 7], [44.68864, -63.60753, 3, 2, 8], [44.66004, -63.55202, 3, 2, 8], [44.6539, -63.54591, 3, 2, 8], [44.69342, -63.50028, 4, 2, 8], [44.64373, -63.57439, 0, 2, 8], [44.68087, -63.4903, 3, 2, 8], [44.70847, -63.68376, 3, 2, 8], [44.7675, -63.69425, 1, 2, 8], [44.65117, -63.65194, 2, 2, 8], [44.70053, -63.68134, 3, 2, 9], [44.84933, -63.59982, 3, 2, 9], [44.68113, -63.59334, 3, 2, 9], [44.60617, -63.60966, 3, 2, 9], [44.65525, -63.60766, 2, 2, 9], [44.70558, -63.59638, 3, 2, 9], [44.672, -63.67306, 3, 2, 9], [44.68805, -63.53097, 2, 2, 9], [44.75747, -63.66665, 3, 2, 9], [44.64456, -63.59075, 0, 2, 10], [44.67686, -63.49834, 0, 2, 10], [44.68681, -63.58765, 3, 2, 10], [44.68606, -63.5816, 2, 2, 10], [44.76753, -63.68114, 3, 2, 10], [44.60213, -63.6078, 2, 2, 10], [44.62827, -63.57128, 2, 2, 10], [44.7687, -63.68677, 3, 2, 10], [44.6373, -63.57652, 0, 2, 10], [44.65266, -63.586, 0, 2, 10], [44.65128, -63.67358, 0, 2, 10], [44.7594, -63.68208, 3, 2, 10], [44.79591, -63.70696, 3, 2, 10], [44.68434, -63.58351, 3, 2, 10], [44.78511, -63.65305, 2, 2, 10], [44.65454, -63.53109, 3, 2, 10], [44.67204, -63.57729, 3, 2, 10], [44.73117, -63.66004, 3, 2, 10], [44.63863, -63.5758, 0, 2, 10], [44.68492, -63.59853, 3, 2, 10], [44.67041, -63.55082, 3, 2, 11], [44.6875, -63.59047, 1, 2, 11], [44.65454, -63.53109, 0, 2, 11], [44.56533, -63.56298, 3, 2, 11], [44.64503, -63.5741, 3, 2, 11], [44.65746, -63.54217, 2, 2, 11], [44.75095, -63.82158, 1, 2, 11], [44.67237, -63.56617, 0, 2, 11], [44.65992, -63.63329, 3, 2, 11], [44.65442, -63.6471, 3, 2, 11], [44.69078, -63.52839, 3, 2, 11], [44.65134, -63.59959, 3, 2, 11], [44.77084, -63.69888, 3, 2, 11], [44.64981, -63.57396, 2, 2, 11], [44.67316, -63.64872, 0, 2, 11], [44.64008, -64.01555, 2, 2, 11], [44.79376, -63.64227, 0, 2, 11], [44.65301, -63.61008, 0, 2, 11], [44.65585, -63.62291, 3, 2, 11], [44.69985, -63.58668, 0, 2, 12], [44.61587, -63.61193, 0, 2, 12], [44.61587, -63.61193, 0, 2, 12], [44.69269, -63.53257, 3, 2, 12], [44.73215, -63.6477, 3, 2, 12], [44.66986, -63.57774, 3, 2, 12], [44.64077, -63.71239, 3, 2, 12], [44.65578, -63.54989, 3, 2, 12], [44.6362, -63.66937, 0, 2, 12], [44.67108, -63.51001, 0, 2, 12], [44.64856, -63.67498, 0, 2, 12], [44.6764, -63.51421, 0, 2, 12], [44.73387, -63.78838, 3, 2, 12], [44.67806, -63.58943, 2, 2, 12], [44.73072, -63.75745, 0, 2, 12], [44.68606, -63.5816, 3, 2, 12], [44.61587, -63.61193, 0, 2, 12], [44.67763, -63.53858, 0, 2, 12], [44.77211, -63.69376, 0, 2, 13], [44.64439, -63.60076, 3, 2, 13], [44.66863, -63.51542, 0, 2, 13], [44.65277, -63.58524, 0, 2, 13], [44.69985, -63.58668, 1, 2, 13], [44.66153, -63.59846, 0, 2, 13], [44.73504, -63.78342, 3, 2, 13], [44.77963, -63.67905, 2, 2, 13], [44.65289, -63.59245, 0, 2, 13], [44.69078, -63.52839, 4, 2, 13], [44.60685, -63.48782, 3, 2, 13], [44.73823, -63.3876, 3, 2, 13], [44.65633, -63.58414, 3, 2, 13], [44.66871, -63.51666, 0, 2, 14], [44.65364, -63.65068, 0, 2, 14], [44.67406, -63.49454, 0, 2, 14], [44.689, -63.58239, 0, 2, 14], [44.66616, -63.52943, 0, 2, 14], [44.66871, -63.51666, 0, 2, 14], [44.70264, -63.59173, 1, 2, 14], [44.68705, -63.58338, 3, 2, 14], [44.83083, -63.5979, 0, 2, 14], [44.66871, -63.51666, 0, 2, 14], [44.66871, -63.51666, 0, 2, 14], [44.68969, -63.89823, 2, 2, 14], [44.81763, -63.74255, 3, 2, 14], [44.65197, -63.60658, 3, 2, 14], [44.70766, -63.55167, 2, 2, 15], [44.75616, -63.73403, 3, 2, 15], [44.65004, -63.58054, 3, 2, 15], [44.685, -63.5852, 3, 2, 15], [44.71693, -63.60099, 0, 2, 15], [44.65766, -63.60504, 3, 2, 15], [44.70711, -63.47482, 1, 2, 15], [44.65933, -63.6669, 3, 2, 15], [44.68568, -63.58629, 2, 2, 15], [44.66994, -63.51895, 0, 2, 15], [44.83503, -63.61779, 2, 2, 15], [44.60315, -63.60908, 3, 2, 15], [44.68915, -63.58684, 1, 2, 15], [44.66075, -63.50428, 0, 2, 15], [44.66525, -63.54826, 3, 2, 15], [44.8479, -63.69137, 1, 4, 4], [44.64503, -63.5741, 3, 4, 4], [44.68849, -63.58332, 3, 4, 4], [44.67419, -63.5772, 2, 4, 4], [44.67178, -63.57185, 3, 4, 4], [44.64065, -63.58138, 3, 4, 4], [44.72554, -63.54053, 3, 4, 4], [44.65204, -63.58732, 4, 4, 4], [44.67025, -63.60594, 3, 4, 4], [44.66653, -63.53983, 2, 4, 4], [44.60048, -63.6835, 1, 4, 4], [44.67728, -63.52761, 3, 4, 4], [44.59168, -63.6981, 0, 4, 4], [44.79555, -63.72748, 3, 4, 4], [44.64714, -63.61901, 3, 4, 4], [44.77355, -63.68326, 3, 4, 5], [44.65004, -63.58054, 3, 4, 5], [44.66846, -63.60901, 0, 4, 5], [44.64714, -63.67413, 0, 4, 5], [44.6655, -63.66403, 3, 4, 5], [44.6458, -63.57191, 2, 4, 5], [44.65762, -63.64196, 0, 4, 5], [44.80196, -62.67579, 2, 4, 5], [44.65495, -63.62992, 3, 4, 5], [44.62972, -63.62517, 3, 4, 5], [44.69078, -63.52839, 3, 4, 5], [44.77965, -63.66908, 0, 4, 5], [44.6699, -63.58227, 1, 4, 5], [44.76351, -63.69915, 3, 4, 5], [44.6664, -63.76147, 0, 4, 5], [44.64705, -63.57696, 3, 4, 5], [44.59688, -63.6024, 3, 4, 5], [44.66818, -63.56087, 3, 4, 5], [44.78639, -63.68464, 3, 4, 5], [44.685, -63.5852, 3, 4, 5], [44.77847, -63.66787, 0, 4, 5], [44.6655, -63.66403, 3, 4, 5], [44.62972, -63.62517, 3, 4, 5], [44.62647, -63.59741, 0, 4, 6], [44.65597, -63.58504, 3, 4, 6], [44.64478, -63.57947, 2, 4, 6], [44.59992, -63.60651, 3, 4, 6], [44.68447, -63.58918, 3, 4, 6], [44.75507, -63.23053, 3, 4, 6], [44.66463, -63.64993, 0, 4, 6], [44.70271, -63.68918, 0, 4, 6], [44.78292, -63.59769, 3, 4, 6], [44.59804, -63.4575, 2, 4, 6], [44.61125, -63.48615, 2, 4, 6], [44.70053, -63.68134, 3, 4, 6], [44.65122, -63.57636, 0, 4, 6], [44.68105, -63.58473, 3, 4, 6], [44.47485, -63.57536, 3, 4, 6], [44.69634, -63.68021, 0, 4, 6], [44.73591, -63.6587, 2, 4, 6], [44.65572, -63.64699, 3, 4, 6], [44.67994, -63.58352, 3, 4, 6], [44.65597, -63.58504, 3, 4, 7], [44.67462, -63.4793, 3, 4, 7], [44.96348, -63.29716, 3, 4, 7], [44.85057, -63.60937, 3, 4, 7], [44.66661, -63.55648, 0, 4, 7], [44.67041, -63.57461, 3, 4, 7], [44.67915, -63.58372, 3, 4, 7], [44.64736, -63.53587, 1, 4, 7], [44.66546, -63.50517, 1, 4, 7], [44.68681, -63.58765, 2, 4, 8], [44.77411, -63.68999, 0, 4, 8], [44.65662, -63.54775, 3, 4, 8], [44.68681, -63.58765, 2, 4, 8], [44.66975, -63.56658, 3, 4, 8], [44.6951, -63.57776, 3, 4, 8], [44.65075, -63.60125, 3, 4, 8], [44.6793, -63.54582, 3, 4, 8], [44.76755, -63.66482, 0, 4, 8], [44.68456, -63.58205, 0, 4, 8], [44.63737, -63.57884, 3, 4, 8], [44.64242, -63.57925, 3, 4, 8], [44.65703, -63.59153, 3, 4, 8], [44.71974, -63.5858, 3, 4, 9], [44.95205, -63.49691, 3, 4, 9], [44.67974, -63.53687, 3, 4, 9], [44.62812, -63.43495, 2, 4, 9], [44.57287, -63.63694, 2, 4, 9], [44.68128, -63.58203, 3, 4, 9], [44.66346, -63.54866, 3, 4, 9], [44.78116, -63.70998, 0, 4, 9], [44.70228, -63.66148, 3, 4, 9], [44.60421, -63.47029, 2, 4, 9], [44.67134, -63.56112, 0, 4, 9], [44.8501, -63.69648, 3, 4, 9], [44.63206, -63.57645, 3, 4, 9], [44.63345, -63.57537, 0, 4, 9], [44.62506, -63.61912, 3, 4, 9], [44.63902, -63.57814, 3, 4, 9], [44.66346, -63.54866, 3, 4, 9], [44.65142, -63.58211, 1, 4, 9], [44.68404, -63.5936, 0, 4, 9], [44.78048, -63.67145, 0, 4, 10], [44.68456, -63.58205, 0, 4, 10], [44.65007, -63.54643, 4, 4, 10], [44.79503, -63.72414, 3, 4, 10], [44.68864, -63.60753, 0, 4, 10], [44.68864, -63.60753, 0, 4, 10], [44.65585, -63.62291, 3, 4, 10], [44.68864, -63.60753, 1, 4, 10], [44.67752, -63.5822, 3, 4, 10], [44.83517, -63.57939, 0, 4, 10], [44.70514, -63.67217, 2, 4, 10], [44.67212, -63.58034, 2, 4, 11], [44.72571, -63.39281, 3, 4, 11], [44.65805, -63.63101, 0, 4, 11], [44.67212, -63.58034, 2, 4, 11], [44.63278, -63.57821, 0, 4, 11], [44.6234, -63.57292, 0, 4, 11], [44.67258, -63.57903, 2, 4, 11], [44.67041, -63.57461, 3, 4, 11], [44.6456, -63.62407, 1, 4, 11], [44.6759, -63.65044, 3, 4, 11], [44.67698, -63.57887, 3, 4, 11], [44.65643, -63.58583, 3, 4, 11], [44.65623, -63.59121, 3, 4, 11], [44.67296, -63.58048, 2, 4, 11], [44.68087, -63.4903, 3, 4, 11], [44.68797, -63.52787, 0, 4, 11], [44.62388, -63.57411, 0, 4, 12], [44.636, -63.57589, 0, 4, 12], [44.70518, -63.857, 3, 4, 12], [44.64933, -63.57272, 0, 4, 12], [44.62827, -63.57128, 3, 4, 12], [44.67266, -63.60997, 4, 4, 12], [44.69936, -63.59193, 2, 4, 12], [44.6596, -63.95642, 3, 4, 12], [44.69936, -63.59193, 1, 4, 12], [44.65089, -63.59884, 2, 4, 12], [44.46696, -63.61568, 0, 4, 13], [44.64479, -63.57246, 3, 4, 13], [44.76543, -63.68016, 3, 4, 13], [44.65637, -63.64156, 3, 4, 13], [44.67041, -63.55082, 3, 4, 13], [44.81636, -63.61147, 3, 4, 13], [44.65671, -63.6337, 3, 4, 13], [44.64854, -63.67511, 2, 4, 14], [44.63686, -63.59018, 2, 4, 14], [44.6537, -63.62524, 3, 4, 14], [44.68497, -63.66763, 3, 4, 14], [44.60634, -63.67276, 3, 4, 14], [44.67259, -63.6722, 3, 4, 14], [44.66337, -63.59732, 0, 4, 14], [44.63278, -63.57821, 3, 4, 14], [44.66557, -63.59806, 0, 4, 14], [44.62426, -63.45772, 2, 4, 14], [44.68299, -63.54418, 0, 4, 14], [44.65188, -63.60057, 0, 4, 14], [44.68897, -63.5838, 2, 4, 14], [44.71974, -63.5858, 3, 4, 15], [44.64373, -63.57439, 3, 4, 15], [44.6498, -63.57834, 3, 4, 15], [44.66765, -63.6438, 0, 4, 15], [44.65953, -63.74942, 3, 4, 15], [44.66842, -63.67539, 3, 4, 15], [44.81379, -63.61668, 3, 4, 15], [44.73746, -63.65132, 3, 4, 15], [44.65346, -63.58411, 2, 4, 15], [44.6498, -63.57834, 3, 4, 15], [44.65597, -63.58504, 4, 4, 16], [44.51068, -63.78149, 3, 4, 16], [44.79811, -63.73389, 3, 4, 16], [44.65308, -63.58577, 3, 4, 16], [44.68903, -63.54206, 3, 4, 16], [44.65342, -63.59077, 2, 4, 16], [44.6983, -63.86247, 3, 4, 16], [44.64439, -63.59813, 0, 4, 16], [44.67196, -63.65413, 0, 4, 16], [44.82416, -63.77658, 1, 4, 16], [44.65426, -63.58357, 0, 4, 16], [44.67025, -63.60594, 3, 4, 16], [44.77835, -63.08779, 3, 4, 16], [44.64981, -63.57396, 3, 4, 16], [44.64242, -63.57925, 4, 4, 17], [44.6851, -63.58969, 3, 4, 17], [44.66588, -63.56633, 3, 4, 17], [44.6624, -63.55591, 1, 4, 17], [44.92186, -63.52569, 3, 4, 17], [44.67046, -63.57815, 3, 4, 17], [44.5919, -63.59646, 3, 4, 17], [44.64224, -63.5694, 3, 4, 17], [44.92751, -63.53604, 0, 4, 17], [44.65426, -63.58357, 3, 4, 17], [44.62909, -63.42038, 2, 4, 17], [44.65289, -63.59245, 4, 4, 17], [44.64545, -63.58553, 3, 4, 17], [44.629, -63.63022, 3, 4, 17], [44.69922, -63.66119, 3, 4, 17], [44.65142, -63.48784, 3, 4, 17], [44.67911, -63.55143, 3, 4, 17], [44.6983, -63.86247, 3, 4, 17], [44.66452, -63.56865, 3, 4, 17], [44.64125, -63.57103, 3, 4, 17], [44.65547, -63.58472, 0, 4, 18], [44.64518, -63.62154, 0, 4, 18], [44.69078, -63.52839, 4, 4, 18], [44.71974, -63.5858, 3, 4, 18], [44.7177, -63.70382, 3, 4, 18], [44.65231, -63.58121, 0, 4, 18], [44.64401, -63.5825, 3, 4, 18], [44.64603, -63.59506, 3, 4, 18], [44.66023, -63.6533, 3, 4, 18], [44.65967, -63.60429, 3, 4, 18], [44.64996, -63.57547, 3, 4, 18], [44.63377, -63.57414, 0, 4, 18], [44.63813, -63.57151, 0, 4, 18], [44.73821, -63.30763, 0, 4, 18], [44.67061, -63.58585, 2, 4, 18], [44.6144, -63.91942, 0, 4, 18], [44.64048, -63.57614, 0, 4, 18], [44.65484, -63.5917, 2, 4, 18], [44.64503, -63.5741, 3, 4, 19], [44.62848, -63.60781, 3, 4, 19], [44.6498, -63.57834, 0, 4, 19], [44.66091, -63.54542, 2, 4, 19], [44.63937, -63.59487, 3, 4, 19], [44.63618, -63.57182, 2, 4, 19], [44.6458, -63.57191, 2, 4, 19], [44.67291, -63.58711, 0, 4, 19], [44.64859, -63.61951, 3, 4, 19], [44.63737, -63.57884, 2, 4, 19], [44.60233, -63.60619, 3, 4, 19], [44.63618, -63.57182, 0, 4, 19], [44.65397, -63.62238, 0, 4, 19], [44.64559, -63.57403, 3, 4, 19], [44.68434, -63.58351, 3, 4, 19], [44.68187, -63.54015, 4, 4, 20], [44.69737, -63.5476, 0, 4, 20], [44.66675, -63.57043, 3, 4, 20], [44.74071, -63.46659, 1, 4, 20], [44.68636, -63.58369, 3, 4, 20], [44.65258, -63.63872, 3, 4, 20], [44.79503, -63.72414, 3, 4, 20], [44.72592, -63.39481, 3, 4, 20], [44.61511, -63.48497, 4, 4, 20], [44.68681, -63.58765, 3, 4, 20], [44.63958, -63.27642, 3, 4, 20], [44.68681, -63.58765, 3, 4, 20], [44.67734, -63.80395, 3, 4, 20], [44.65337, -63.58062, 3, 4, 20], [44.67999, -63.59256, 3, 4, 20], [44.66279, -63.5991, 0, 4, 21], [44.66027, -63.63639, 3, 4, 21], [44.65342, -63.59077, 2, 4, 21], [44.66819, -63.48087, 2, 4, 21], [44.76012, -63.66796, 0, 4, 21], [44.68249, -63.51514, 0, 4, 21], [44.65818, -63.63935, 1, 4, 22], [44.77755, -63.68276, 3, 4, 22], [44.75915, -63.66951, 2, 4, 22], [44.65829, -63.73735, 3, 4, 22], [44.66452, -63.56865, 4, 4, 22], [44.65856, -63.59572, 0, 4, 22], [44.65124, -63.54285, 3, 4, 22], [44.64466, -63.59986, 3, 4, 22], [44.67645, -63.48269, 3, 4, 22], [44.64022, -63.58685, 3, 4, 22], [44.74042, -63.30095, 3, 4, 22], [44.66091, -63.63367, 3, 4, 22], [44.67561, -63.52246, 0, 4, 22], [44.67974, -63.52492, 0, 4, 22], [44.62493, -63.60539, 3, 4, 22], [44.65542, -63.63669, 3, 4, 22], [44.62107, -63.61526, 3, 4, 22], [44.75338, -63.83193, 3, 4, 22], [44.71974, -63.5858, 3, 4, 23], [44.65134, -63.59959, 3, 4, 23], [44.63972, -63.57312, 0, 4, 23], [44.6929, -63.56629, 3, 4, 23], [44.7802, -63.68098, 3, 4, 23], [44.67293, -63.56979, 0, 4, 23], [44.65482, -63.58667, 3, 4, 23], [44.64814, -63.62065, 3, 4, 23], [44.65451, -63.64872, 0, 4, 23], [44.6446, -63.59467, 2, 4, 23], [44.7907, -63.73604, 3, 4, 23], [44.67531, -63.58583, 3, 4, 23], [44.7955, -63.62736, 0, 4, 23], [44.65124, -63.98349, 3, 4, 24], [44.72934, -63.67221, 3, 4, 24], [44.63977, -63.57678, 2, 4, 24], [44.80704, -63.62771, 2, 4, 24], [44.65183, -63.48702, 1, 4, 24], [44.63706, -63.52469, 0, 4, 24], [44.76609, -63.68323, 3, 4, 24], [44.7423, -63.5463, 2, 4, 24], [44.68074, -63.48698, 2, 4, 24], [44.6612, -63.58694, 3, 4, 24], [44.68533, -63.56604, 0, 4, 24], [44.67644, -63.48111, 4, 4, 24], [44.68647, -63.66416, 2, 4, 24], [44.67998, -63.58713, 3, 4, 24], [44.77533, -63.67371, 3, 4, 24], [44.83138, -62.6231, 0, 4, 24], [44.70343, -63.58889, 1, 4, 24], [44.66051, -63.55025, 3, 4, 24], [44.64772, -63.57433, 3, 4, 24], [44.6012, -63.60823, 2, 4, 24], [44.67249, -63.53485, 0, 4, 24], [44.68606, -63.5816, 3, 4, 25], [44.62508, -63.60007, 0, 4, 25], [44.65007, -63.60784, 0, 4, 25], [44.68371, -63.5021, 3, 4, 25], [44.64072, -63.5752, 3, 4, 25], [44.64398, -63.67471, 2, 4, 25], [44.67394, -63.58578, 3, 4, 25], [44.66362, -63.61268, 0, 4, 25], [44.59992, -63.60651, 3, 4, 25], [44.69074, -63.58801, 0, 4, 25], [44.65597, -63.58504, 0, 4, 25], [44.65689, -63.59809, 3, 4, 25], [44.72221, -62.83899, 3, 4, 25], [44.67184, -63.60638, 0, 4, 25], [44.65443, -63.58738, 3, 4, 25], [44.94948, -62.24958, 2, 4, 25], [44.62442, -63.59776, 0, 4, 25], [44.76212, -63.67371, 1, 4, 26], [44.64002, -63.57804, 2, 4, 26], [44.63614, -63.57846, 2, 4, 26], [44.6483, -63.65966, 3, 4, 26], [44.66546, -63.55781, 2, 4, 26], [44.68864, -63.60753, 0, 4, 26], [44.67903, -63.5493, 4, 4, 26], [44.61227, -63.48831, 3, 4, 26], [44.64755, -63.57503, 3, 4, 26], [44.66766, -63.60038, 3, 4, 26], [45.09574, -62.87193, 2, 4, 26], [44.62442, -63.59776, 0, 4, 26], [44.60836, -63.47937, 3, 4, 26], [44.69121, -63.48671, 3, 4, 26], [44.68606, -63.5816, 0, 4, 26], [44.62827, -63.57128, 0, 4, 27], [44.63958, -63.27642, 3, 4, 27], [44.68313, -63.58486, 0, 4, 27], [44.6636, -63.64431, 3, 4, 27], [44.70346, -63.85519, 3, 4, 27], [44.67811, -63.54172, 0, 4, 27], [44.66755, -63.54339, 3, 4, 27], [44.62516, -63.61712, 3, 4, 27], [44.68434, -63.58351, 3, 4, 27], [44.77886, -63.67566, 1, 4, 27], [44.65442, -63.6471, 2, 4, 27], [44.66588, -63.56633, 3, 4, 27], [44.6809, -63.58787, 3, 4, 28], [44.74781, -63.29525, 2, 4, 28], [44.66653, -63.60608, 2, 4, 28], [44.66512, -63.54097, 0, 4, 28], [44.66653, -63.60608, 0, 4, 28], [44.65445, -63.64591, 0, 4, 28], [44.67041, -63.57461, 3, 4, 28], [44.68988, -63.52925, 3, 4, 28], [44.66512, -63.54939, 0, 4, 28], [44.68647, -63.66416, 0, 4, 28], [44.70014, -63.6862, 2, 4, 28], [44.66389, -63.60201, 0, 4, 28], [44.78419, -63.67999, 0, 4, 28], [44.62691, -63.4324, 0, 4, 28], [44.70494, -63.61061, 0, 4, 28], [44.64996, -63.57547, 3, 4, 28], [44.66767, -63.55667, 0, 4, 28], [44.68168, -63.54155, 3, 4, 28], [44.67513, -63.59042, 3, 4, 28], [44.64373, -63.57439, 3, 4, 29], [44.72871, -63.64541, 2, 4, 29], [44.64738, -63.67637, 0, 4, 29], [44.68168, -63.49212, 3, 4, 29], [44.72201, -63.69802, 3, 4, 29], [44.64736, -63.53587, 3, 4, 29], [44.65356, -63.59961, 3, 4, 29], [44.63329, -63.70446, 3, 4, 29], [44.70343, -63.58889, 2, 4, 29], [44.73689, -63.32533, 0, 4, 29], [44.70604, -63.58205, 3, 4, 29], [44.61267, -63.61887, 3, 4, 29], [44.643, -63.60068, 2, 4, 29], [44.76212, -63.67371, 2, 4, 30], [44.64172, -63.58184, 2, 4, 30], [44.65596, -63.60738, 3, 4, 30], [44.67041, -63.55082, 2, 4, 30], [44.67936, -63.49672, 2, 4, 30], [44.6536, -63.644, 0, 4, 30], [44.67709, -63.48835, 3, 4, 30], [44.79028, -63.68316, 0, 4, 30], [44.67041, -63.55082, 2, 4, 30], [44.67969, -63.58563, 1, 4, 30], [44.65476, -63.64271, 0, 4, 31], [44.64098, -63.60195, 2, 4, 31], [44.6498, -63.57834, 0, 4, 31], [44.67642, -63.54449, 3, 4, 31], [44.76444, -63.68736, 3, 4, 31], [44.71974, -63.5858, 3, 4, 31], [44.67914, -63.59409, 2, 4, 31], [44.83882, -63.65624, 3, 4, 31], [44.73689, -63.32533, 0, 4, 31], [44.67212, -63.58034, 2, 4, 31], [44.79939, -63.68545, 3, 4, 31], [44.78339, -63.68807, 0, 4, 31], [44.66706, -63.60305, 3, 4, 31], [44.63214, -63.66198, 0, 4, 31], [44.66776, -63.60805, 2, 4, 31], [44.65686, -63.63127, 1, 4, 31], [44.81796, -63.6128, 3, 4, 31], [44.65487, -63.58754, 4, 4, 31], [44.67568, -63.59162, 3, 5, 1], [44.66699, -63.5458, 3, 5, 1], [44.64096, -63.61509, 3, 5, 1], [44.759, -63.65803, 3, 5, 1], [44.78201, -63.70241, 2, 5, 1], [44.6458, -63.57191, 0, 5, 1], [44.66593, -63.54544, 3, 5, 1], [44.67374, -63.58966, 3, 5, 1], [44.69741, -63.68871, 1, 5, 1], [44.60591, -63.47885, 3, 5, 1], [44.67511, -63.57805, 2, 5, 1], [44.70355, -63.70868, 3, 5, 1], [44.75792, -63.6604, 3, 5, 1], [44.68447, -63.58918, 3, 5, 1], [44.64157, -63.58015, 3, 5, 1], [44.64814, -63.62065, 4, 5, 1], [44.65597, -63.58504, 0, 5, 2], [44.65994, -63.67199, 3, 5, 2], [44.61267, -63.61887, 3, 5, 2], [44.65597, -63.58504, 3, 5, 2], [44.65375, -63.64651, 3, 5, 2], [44.67041, -63.57461, 4, 5, 2], [44.66452, -63.56865, 3, 5, 2], [44.6862, -63.50246, 3, 5, 2], [44.70558, -63.59638, 0, 5, 2], [45.18445, -62.87962, 3, 5, 2], [44.61465, -63.61581, 3, 5, 2], [44.6619, -63.48906, 3, 5, 2], [44.91092, -62.53493, 3, 5, 2], [44.77507, -63.35528, 3, 5, 2], [44.64503, -63.5741, 3, 5, 2], [44.65625, -63.585, 3, 5, 3], [44.78949, -63.69002, 3, 5, 3], [44.6833, -63.51304, 4, 5, 3], [44.67894, -63.58471, 3, 5, 3], [44.72863, -63.23541, 2, 5, 3], [44.77732, -63.62813, 1, 5, 3], [44.64401, -63.5825, 3, 5, 3], [44.66261, -63.63285, 3, 5, 3], [44.69167, -63.58368, 3, 5, 3], [44.67437, -63.66681, 0, 5, 3], [44.77979, -63.69518, 1, 5, 3], [44.63545, -63.57647, 2, 5, 4], [44.79589, -63.72972, 1, 5, 4], [44.65458, -63.59746, 0, 5, 4], [44.70079, -63.66746, 3, 5, 4], [44.64072, -63.5752, 3, 5, 4], [44.74701, -63.38365, 3, 5, 4], [44.65992, -63.63329, 3, 5, 4], [44.71974, -63.5858, 3, 5, 4], [44.66971, -63.54861, 2, 5, 4], [44.67514, -63.57709, 2, 5, 4], [44.67514, -63.57709, 2, 5, 4], [44.70604, -63.58205, 3, 5, 4], [44.67419, -63.5772, 2, 5, 4], [44.63774, -63.57455, 1, 5, 4], [44.68879, -63.51253, 1, 5, 5], [44.64695, -63.54046, 2, 5, 5], [44.60367, -63.67546, 3, 5, 5], [44.65578, -63.54989, 3, 5, 5], [44.70604, -63.58205, 3, 5, 5], [44.64098, -63.57694, 2, 5, 5], [44.63383, -63.70586, 0, 5, 5], [44.69078, -63.52839, 4, 5, 5], [44.6698, -63.57701, 3, 5, 5], [44.65443, -63.99323, 0, 5, 5], [44.62827, -63.57128, 1, 5, 5], [44.67851, -63.58381, 0, 5, 6], [44.76469, -63.7864, 3, 5, 6], [44.67406, -63.49454, 2, 5, 6], [44.66512, -63.54097, 3, 5, 6], [44.71974, -63.5858, 3, 5, 6], [44.7687, -63.68677, 3, 5, 6], [44.63465, -63.57802, 0, 5, 6], [44.66136, -63.64225, 3, 5, 6], [44.64121, -63.62457, 4, 5, 6], [44.66618, -63.57117, 4, 5, 6], [44.6663, -63.59909, 3, 5, 6], [44.69014, -63.53266, 0, 5, 6], [44.64072, -63.5752, 3, 5, 6], [44.66116, -63.63998, 0, 5, 6], [44.65367, -63.59488, 0, 5, 6], [44.74713, -63.63604, 3, 5, 6], [44.72555, -63.70335, 0, 5, 7], [44.65768, -63.60501, 0, 5, 7], [44.55377, -63.56149, 1, 5, 7], [44.72199, -63.64813, 0, 5, 7], [44.65134, -63.59959, 2, 5, 7], [44.67872, -63.52169, 3, 5, 7], [44.65056, -63.61279, 1, 5, 7], [44.6617, -63.61424, 3, 5, 7], [44.65056, -63.61279, 2, 5, 7], [44.64185, -63.59608, 2, 5, 7], [44.65633, -63.58414, 0, 5, 7], [44.66063, -63.62992, 3, 5, 7], [44.63833, -63.57347, 2, 5, 7], [44.6422, -63.58005, 2, 5, 7], [44.6801, -63.52162, 3, 5, 7], [44.65693, -63.53189, 0, 5, 7], [44.6272, -63.92376, 2, 5, 7], [44.68764, -63.54066, 0, 5, 7], [44.6289, -63.62789, 3, 5, 7], [44.70604, -63.58205, 3, 5, 8], [44.65597, -63.58504, 2, 5, 8], [44.74903, -63.74298, 3, 5, 8], [44.6875, -63.59047, 0, 5, 8], [44.73345, -63.23675, 3, 5, 8], [44.67694, -63.51627, 3, 5, 8], [44.70014, -63.6862, 2, 5, 8], [44.74334, -63.46753, 2, 5, 8], [44.65597, -63.58504, 2, 5, 8], [44.64786, -63.57737, 4, 5, 8], [44.69857, -63.56738, 3, 5, 8], [44.64698, -63.59356, 3, 5, 8], [44.66104, -63.6309, 3, 5, 8], [44.73823, -63.3876, 0, 5, 8], [44.61308, -63.62385, 0, 5, 8], [44.70404, -63.4512, 3, 5, 8], [44.63026, -63.62179, 1, 5, 8], [44.61588, -63.48228, 3, 5, 8], [44.6818, -63.58632, 1, 5, 8], [44.68681, -63.58765, 0, 5, 8], [44.67851, -63.58381, 0, 5, 8], [44.63774, -63.57455, 4, 5, 8], [44.65597, -63.58504, 0, 5, 8], [44.73823, -63.3876, 0, 5, 8], [44.60528, -63.61307, 3, 5, 8], [44.66915, -63.60085, 3, 5, 9], [44.68128, -63.58203, 3, 5, 9], [44.71752, -63.67979, 3, 5, 9], [44.68128, -63.58203, 3, 5, 9], [44.70014, -63.6862, 2, 5, 9], [44.66361, -63.55532, 4, 5, 9], [44.67316, -63.58439, 3, 5, 9], [44.65123, -63.64972, 0, 5, 9], [44.68568, -63.58629, 3, 5, 9], [44.70617, -63.612, 0, 5, 9], [44.76024, -63.66385, 3, 5, 9], [44.65286, -63.60606, 0, 5, 9], [44.71873, -63.65428, 0, 5, 9], [44.7687, -63.68677, 2, 5, 9], [44.47887, -63.60285, 1, 5, 9], [44.6791, -63.58728, 3, 5, 9], [44.71752, -63.67979, 3, 5, 9], [44.65411, -63.58643, 3, 5, 9], [44.66845, -63.38231, 0, 5, 9], [44.75338, -63.83193, 3, 5, 9], [44.64157, -63.58015, 3, 5, 9], [44.75797, -63.67378, 3, 5, 10], [44.73709, -63.64888, 0, 5, 10], [44.7103, -63.6701, 2, 5, 10], [44.67328, -63.58589, 0, 5, 10], [44.66168, -63.65938, 3, 5, 10], [44.67707, -63.54805, 3, 5, 10], [44.63246, -63.6368, 3, 5, 10], [44.63774, -63.57455, 0, 5, 10], [44.77173, -63.57626, 0, 5, 10], [44.64478, -63.57947, 2, 5, 10], [44.64722, -63.57193, 0, 5, 10], [44.76543, -63.68016, 3, 5, 10], [44.64738, -63.57566, 3, 5, 10], [44.6539, -63.54591, 3, 5, 10], [44.71974, -63.5858, 3, 5, 10], [44.67731, -63.58677, 4, 5, 10], [44.65443, -63.99323, 2, 5, 10], [44.70264, -63.59173, 2, 5, 11], [44.67974, -63.53687, 4, 5, 11], [44.66242, -63.65604, 4, 5, 11], [44.67348, -63.65081, 3, 5, 11], [44.67041, -63.55082, 2, 5, 11], [44.66619, -63.57041, 2, 5, 11], [44.64123, -63.57599, 0, 5, 11], [44.77411, -63.68999, 3, 5, 11], [44.67399, -63.5538, 2, 5, 11], [44.65374, -63.62622, 0, 5, 11], [44.70604, -63.58205, 3, 5, 11], [44.67529, -63.6632, 2, 5, 11], [44.64992, -63.59196, 2, 5, 11], [44.69947, -63.6062, 0, 5, 11], [44.9244, -63.77641, 3, 5, 11], [44.75792, -63.6604, 3, 5, 11], [44.67529, -63.6632, 2, 5, 11], [44.68297, -63.59749, 3, 5, 11], [44.71974, -63.5858, 3, 5, 11], [44.67062, -63.57239, 3, 5, 12], [44.6639, -63.66038, 3, 5, 12], [44.6639, -63.66038, 3, 5, 12], [44.645, -63.57293, 3, 5, 12], [44.71033, -63.57175, 2, 5, 12], [44.68568, -63.58629, 1, 5, 12], [44.69828, -63.54541, 1, 5, 12], [44.66923, -63.57635, 4, 5, 12], [44.68944, -63.56286, 0, 5, 12], [44.68605, -63.53007, 3, 5, 12], [44.63858, -63.58831, 2, 5, 12], [44.68336, -63.55928, 0, 5, 12], [44.68044, -63.59317, 4, 5, 12], [44.67041, -63.55082, 2, 5, 13], [44.71974, -63.5858, 3, 5, 13], [44.67922, -63.66199, 0, 5, 13], [44.68627, -63.56043, 3, 5, 13], [44.69119, -63.58383, 3, 5, 13], [44.69859, -63.60421, 0, 5, 13], [44.67975, -63.66138, 0, 5, 13], [44.64757, -63.58383, 3, 5, 13], [44.68297, -63.59749, 0, 5, 13], [44.92693, -63.53307, 3, 5, 13], [44.67209, -63.57895, 2, 5, 13], [44.70728, -63.58824, 2, 5, 14], [44.69559, -63.54743, 4, 5, 14], [44.66189, -63.63843, 3, 5, 14], [44.76543, -63.68016, 0, 5, 14], [44.67513, -63.59042, 4, 5, 14], [44.67383, -63.49942, 0, 5, 14], [44.68044, -63.59317, 0, 5, 14], [44.72, -63.56245, 2, 5, 14], [44.67067, -63.65854, 0, 5, 14], [44.65597, -63.58504, 3, 5, 14], [44.60324, -63.61256, 3, 5, 14], [44.66609, -63.56489, 3, 5, 14], [44.72359, -63.565, 2, 5, 14], [44.66196, -63.66464, 1, 5, 14], [44.67313, -63.61208, 3, 5, 14], [44.70604, -63.58205, 3, 5, 14], [44.66199, -63.27452, 1, 5, 14], [44.69968, -63.66519, 3, 5, 15], [44.71737, -63.5458, 3, 5, 15], [44.93436, -62.49771, 2, 5, 15], [44.64719, -63.67254, 0, 5, 15], [44.62847, -63.49308, 3, 5, 15], [44.68313, -63.58486, 3, 5, 15], [44.64394, -63.57678, 4, 5, 15], [44.67114, -63.51213, 3, 5, 15], [44.74295, -63.3066, 3, 5, 15], [44.64573, -63.54154, 3, 5, 15], [44.7222, -63.74359, 3, 5, 15], [44.60324, -63.61256, 1, 5, 15], [44.6653, -63.61042, 4, 5, 15], [44.68728, -63.48788, 3, 5, 15], [44.64968, -63.66814, 0, 5, 15], [44.65062, -63.59189, 3, 5, 15], [44.69815, -63.66639, 2, 5, 15], [44.67041, -63.57461, 3, 5, 15], [44.84117, -63.68172, 1, 5, 15], [44.64705, -63.59101, 3, 5, 15], [44.67694, -63.51627, 2, 5, 16], [44.63465, -63.57802, 0, 5, 16], [44.65155, -63.61212, 0, 5, 16], [44.65546, -63.60668, 0, 5, 16], [44.63329, -63.62005, 1, 5, 16], [44.73682, -63.7943, 3, 5, 16], [44.72199, -63.64813, 0, 5, 16], [44.72199, -63.64813, 0, 5, 16], [44.63858, -63.60414, 0, 5, 16], [44.65054, -63.61071, 0, 5, 16], [44.64979, -63.57466, 0, 5, 16], [44.7366, -63.64491, 1, 5, 16], [44.69872, -63.58557, 3, 5, 16], [44.65222, -63.59426, 3, 5, 16], [44.65446, -63.60123, 3, 5, 16], [44.64706, -63.57255, 1, 5, 16], [44.68128, -63.58203, 3, 5, 16], [44.76923, -63.00494, 3, 5, 16], [44.92137, -63.56942, 3, 5, 17], [44.59967, -63.61173, 3, 5, 17], [44.66411, -63.61271, 3, 5, 17], [44.60347, -63.60742, 3, 5, 17], [44.70445, -63.6798, 4, 5, 17], [44.64031, -63.57678, 2, 5, 17], [44.67258, -63.57903, 3, 5, 17], [44.68681, -63.58765, 3, 5, 17], [44.69167, -63.58368, 3, 5, 17], [44.65335, -63.59675, 3, 5, 17], [44.69167, -63.58368, 3, 5, 17], [44.65578, -63.54989, 3, 5, 17], [44.65605, -63.58926, 3, 5, 18], [44.68681, -63.58765, 2, 5, 18], [44.65855, -63.55097, 2, 5, 18], [44.65082, -63.57433, 2, 5, 18], [44.57287, -63.63694, 2, 5, 18], [44.76212, -63.67371, 2, 5, 18], [44.80101, -63.72278, 3, 5, 18], [44.69643, -63.60218, 0, 5, 18], [44.98472, -63.45142, 3, 5, 18], [44.64033, -63.58481, 2, 5, 18], [44.68864, -63.60753, 0, 5, 18], [44.75792, -63.6604, 3, 5, 18], [44.66377, -63.64817, 0, 5, 18], [44.65186, -63.65165, 3, 5, 18], [44.64816, -63.59888, 0, 5, 18], [44.65031, -63.58395, 0, 5, 19], [44.71046, -63.84201, 0, 5, 19], [44.66477, -63.53804, 3, 5, 19], [44.65514, -63.58737, 3, 5, 19], [44.65514, -63.58737, 3, 5, 19], [44.70067, -63.54612, 0, 5, 19], [44.67258, -63.57903, 3, 5, 19], [44.70065, -63.5445, 0, 5, 19], [44.68447, -63.58918, 3, 5, 19], [44.70065, -63.5445, 0, 5, 19], [44.78201, -63.70241, 3, 5, 19], [44.70067, -63.54612, 0, 5, 19], [44.71627, -63.70849, 3, 5, 19], [44.68258, -63.52396, 3, 5, 19], [44.66692, -63.56806, 3, 5, 19], [44.63823, -63.56981, 3, 5, 19], [44.71033, -63.57175, 0, 5, 19], [44.68501, -63.58034, 3, 5, 19], [44.65578, -63.54989, 3, 5, 19], [44.64937, -63.60813, 0, 5, 19], [44.71918, -63.81936, 3, 5, 19], [44.66313, -63.6392, 0, 5, 20], [44.65406, -63.55156, 2, 5, 20], [44.65578, -63.54989, 3, 5, 20], [44.63738, -63.56969, 3, 5, 20], [44.67258, -63.57903, 3, 5, 20], [44.72748, -63.67658, 0, 5, 20], [44.86725, -63.68747, 3, 5, 20], [44.6655, -63.66403, 2, 5, 20], [44.759, -63.65803, 3, 5, 20], [44.76455, -63.64944, 3, 5, 20], [44.65926, -63.63182, 2, 5, 20], [44.64834, -63.60664, 0, 5, 20], [44.66832, -63.54536, 0, 5, 20], [44.66627, -63.56293, 2, 5, 20], [44.78528, -63.67825, 0, 5, 20], [44.69078, -63.52839, 3, 5, 20], [44.6539, -63.54591, 3, 5, 21], [44.72777, -63.67148, 2, 5, 21], [44.65487, -63.58754, 2, 5, 21], [44.63885, -63.59997, 3, 5, 21], [44.70681, -63.55143, 0, 5, 21], [44.74982, -63.66071, 3, 5, 21], [44.75792, -63.6604, 3, 5, 21], [44.67336, -63.66901, 0, 5, 21], [44.65257, -63.62516, 2, 5, 21], [44.67903, -63.5493, 2, 5, 21], [44.61308, -63.62385, 4, 5, 21], [44.75792, -63.6604, 3, 5, 21], [44.75792, -63.6604, 3, 5, 21]],
      'year': 2018
    };

    // transform and inflate dates from server and create mock times so that each days data is at least somewhat distributed
    let year = Data.year;
    const crimes = Data.crimes;
    let hourCounter, month, currentTimeStamp, currentDay = 0;
    for (let i = 0, len = crimes.length; i < len; i++) {
      const crime = crimes[i];

      if (currentDay !== crime[4]) {

        // check for crossing year
        if (month > crime[3]) {
          year += 1;
        }

        month = crime[3];
        hourCounter = 10;
        currentDay = crime[4];

        currentTimeStamp = Date.parse(year + '/' + (month + 1) + '/' + currentDay);
      }
      // auto generate hour to spread the crimes over the course of the day and get a smoother animation
      crime[3] = currentTimeStamp + (hourCounter * (DAY_IN_MILLI / 40));
      hourCounter += 1;
    }


  }
}
